package com.ly.subway.subwaytools.dsfstarter.utils;

import com.ly.spat.dsf.kernel.ServiceConfig;
import com.ly.subway.subwaytools.dsfserver.startup.DSFApplicationEnv;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class WebSocketFrameHandler
        extends SimpleChannelInboundHandler<TextWebSocketFrame>

{


    @Override
    protected void channelRead0(ChannelHandlerContext ctx, TextWebSocketFrame msg) throws Exception {

        String request = msg.text();
        if ("get".equals(request)) {
            Class<?> cls = Class.forName("com.ly.spat.dsf.kernel.ServiceConfig", true, Thread.currentThread().getContextClassLoader());
            Method method = cls.getMethod("getBody");
            method.setAccessible(true);
            sendContract(ctx, method);
        }
    }

    private void sendContract(ChannelHandlerContext ctx, Method method)
            throws IllegalAccessException, InvocationTargetException {
        try {
//            ClassLoader loader = Thread.currentThread().getContextClassLoader();
//            Class<?> envClass = Class.forName("com.ly.spat.dsf.startup.DSFApplicationEnv", true, loader);
//
//            Method applicationMethod = envClass.getMethod("getContext");
//            applicationMethod.setAccessible(true);
//            Constructor<?> c = envClass.getDeclaredConstructor();
//            c.setAccessible(true);
//
//            Object obj = applicationMethod.invoke(c.newInstance());
//            Class<?> objClazz = obj.getClass();
//
//            Method beanMethod = objClazz.getMethod("getBean", String.class);
//            String body = (String) method.invoke(beanMethod.invoke(obj, "com.ly.spat.dsf.kernel.ServiceConfig"));
            ServiceConfig serviceConfig= DSFApplicationEnv.getContext().getBean(ServiceConfig.class);
            String body=serviceConfig.getBody();
            System.out.println(body);
            ctx.channel().writeAndFlush(new TextWebSocketFrame(body));
        } catch (Exception e) {
            try {
                System.out.println("retry get contract");
                Thread.sleep(500);
                sendContract(ctx, method);
            } catch (InterruptedException e1) {
            }
        }
    }
}


