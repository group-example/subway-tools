package com.ly.subway.subwaytools.dsfstarter.autoconfig;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 *
 *
 *
 *
 * <p>
 * 修改历史:                                                                                    &lt;br&gt;
 * 修改日期             修改人员        版本                     修改内容
 * --------------------------------------------------------------------
 * 2018年07月09日 上午11:20   shikai.liu     1.0   初始化创建
 * </p>
 *
 * @author shikai.liu
 * @version 1.0
 * @since JDK1.7
 */
@Configuration
//@PropertySource("jar-version.properties")
public class JarversionConfig {
//	@Value("${dsfstart.version:}")
	private String dsfstart="1.1.0";
	@Bean
	public JarversionConfig getConfig()
	{
		return new JarversionConfig();
	}
}