package com.ly.subway.subwaytools.dsfstarter.autoconfig;

import com.ly.spat.dsf.annoation.DSFService;
import com.ly.spat.dsf.client.proxy.ProxyFactory;
import com.ly.spat.dsf.utils.ObjectHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Field;
import java.util.List;

/**
 *
 *
 *
 *
 *
 * <p>
 * 修改历史:                                                                                    &lt;br&gt;
 * 修改日期             修改人员        版本                     修改内容
 * --------------------------------------------------------------------
 * 2018年07月09日 上午11:16   shikai.liu     1.0   初始化创建
 * </p>
 *
 * @author shikai.liu
 * @version 1.0
 * @since JDK1.7
 */

/**
 * 初始化DsfClient属性
 */
@Component
public class DSFBeanPostProcessor implements BeanPostProcessor {
	private static final Logger log = LoggerFactory.getLogger(DSFBeanPostProcessor.class);
	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		List<Field> list = CollectionUtils.arrayToList(bean.getClass().getDeclaredFields());
		list.stream().forEach(field -> {
			DSFClient consumer = field.getAnnotation(DSFClient.class);
			if (consumer != null) {
				String groupName = consumer.group();
				String version = consumer.version();
				DSFService service = field.getType().getDeclaredAnnotation(DSFService.class);
				ObjectHelper.requireNonNull(service, "DSFService can not be null");
				String sname = service.value();

				field.setAccessible(true);
				try {
					field.set(bean, ProxyFactory.getProxy(field.getType(), groupName, sname, version));
					log.info("bean processor success. bean type:{} , field:{}", bean.getClass().getName(), field.getName());
				} catch (Exception e) {
					log.error("postProcessBeforeInitialization error",e);
				}
			}
		});
		return bean;
	}
	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		return bean;
	}
}
