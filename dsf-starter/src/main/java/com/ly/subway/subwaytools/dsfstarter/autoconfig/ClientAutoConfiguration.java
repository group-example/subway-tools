package com.ly.subway.subwaytools.dsfstarter.autoconfig;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 *
 *
 *
 *
 *
 * <p>
 * 修改历史:                                                                                    &lt;br&gt;
 * 修改日期             修改人员        版本                     修改内容
 * --------------------------------------------------------------------
 * 2018年07月09日 上午11:11   shikai.liu     1.0   初始化创建
 * </p>
 *
 * @author shikai.liu
 * @version 1.0
 * @since JDK1.7
 */
@Configuration
public class ClientAutoConfiguration {
	@Value("${dsf.service.config.name:}")
	private String cname;
	@Value("${dsf.service.config.registryhost:}")
	private String registry;
	@Value("${dsf.service.config.env:}")
	private String env;
//	@Bean
//	public DSFProxy DsfProxy() {
//		ObjectHelper.requireNonNull(registry, "center host can not be empty");
//		ObjectHelper.requireNonNull(cname, "consumer name can not be empty");
//		return new DSFProxy.Builder().setCName(cname).setEnv(env).setRegistry(registry).build();
//	}
}
