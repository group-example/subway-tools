package com.ly.subway.subwaytools.dsfstarter.utils;

import com.google.gson.annotations.SerializedName;

/**
 * 公共常量类定义
 * <p>
 * <p>
 * 修改历史:											<br>
 * 修改日期    		修改人员   	版本	 		修改内容<br>
 * -------------------------------------------------<br>
 * 2018/7/24   hui.zhao     1.0    	初始化创建<br>
 * </p>
 *
 * @author hui.zhao
 * @version 1.0
 * @since JDK1.7
 */
public class ToolEntity {


    @SerializedName("DownloadUrl")
    private String downloadUrl;

    @SerializedName("Version")
    private String version;

    @SerializedName("UpdateTime")
    private String updateTime;

    @SerializedName("FileMD5")
    private String fileMD5;

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getFileMD5() {
        return fileMD5;
    }

    public void setFileMD5(String fileMD5) {
        this.fileMD5 = fileMD5;
    }


}
