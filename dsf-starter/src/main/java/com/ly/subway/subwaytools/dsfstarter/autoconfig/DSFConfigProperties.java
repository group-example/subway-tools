package com.ly.subway.subwaytools.dsfstarter.autoconfig;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 *
 *
 *
 *
 *
 * <p>
 * 修改历史:                                                                                    &lt;br&gt;
 * 修改日期             修改人员        版本                     修改内容
 * --------------------------------------------------------------------
 * 2018年07月09日 上午11:17   shikai.liu     1.0   初始化创建
 * </p>
 *
 * @author shikai.liu
 * @version 1.0
 * @since JDK1.7
 */
@Component
@ConfigurationProperties(prefix = "dsf")
public class DSFConfigProperties {
	private Service service = new Service();
	private Kernel kernel = new Kernel();
	private Server server = new Server();
	public Service getService() {
		return service;
	}
	public void setService(Service service) {
		this.service = service;
	}
	public Kernel getKernel() {
		return kernel;
	}
	public void setKernel(Kernel kernel) {
		this.kernel = kernel;
	}
	public Server getServer() {
		return server;
	}
	public void setServer(Server server) {
		this.server = server;
	}
	public static class Service {
		private Config config = new Config();
		public Config getConfig() {
			return config;
		}
		public void setConfig(Config config) {
			this.config = config;
		}
		public static class Config {
			private String name;
			private String version;
			private boolean isKernel;
			private String registryhost;
			private String env;
			public String getName() {
				Objects.requireNonNull(name, "服务组名不可为空，请检查dsf配置");
				return name;
			}
			public void setName(String name) {
				this.name = name;
			}
			public String getVersion() {
				Objects.requireNonNull(version, "版本号不可为空，请检查dsf配置");
				return version;
			}
			public void setVersion(String version) {
				this.version = version;
			}
			public boolean isKernel() {
				return isKernel;
			}
			public void setKernel(boolean kernel) {
				isKernel = kernel;
			}
			public String getRegistryhost() {
				return registryhost;
			}
			public void setRegistryhost(String registryhost) {
				this.registryhost = registryhost;
			}
			public String getEnv() {
				return env;
			}
			public void setEnv(String env) {
				this.env = env;
			}
		}
	}
	public static class Server {
		private int port;
		private int accepts = 50;
		private int threads = 200;
		public int getAccepts() {
			return accepts;
		}
		public void setAccepts(int accepts) {
			this.accepts = accepts;
		}
		public int getThreads() {
			return threads;
		}
		public void setThreads(int threads) {
			this.threads = threads;
		}
		public int getPort() {
			return port;
		}
		public void setPort(int port) {
			this.port = port;
		}
	}
	public static class Kernel {
		private Config config = new Config();
		public Config getConfig() {
			return config;
		}
		public void setConfig(Config config) {
			this.config = config;
		}
		public static class Config {
			private String host;
			private int port;
			public String getHost() {
				return host;
			}
			public void setHost(String host) {
				this.host = host;
			}
			public int getPort() {
				return port;
			}
			public void setPort(int port) {
				this.port = port;
			}
		}
	}
}
