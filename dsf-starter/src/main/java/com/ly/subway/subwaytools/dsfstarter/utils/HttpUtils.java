package com.ly.subway.subwaytools.dsfstarter.utils;

import com.ly.spat.dsf.client.handler.HttpUrl;
import com.ly.spat.dsf.client.utils.OKHttpUtils;
import com.ly.spat.dsf.utils.NetUtils;
import com.ly.spat.dsf.utils.StringUtils;

import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * 公共常量类定义
 * <p>
 * <p>
 * 修改历史:											<br>
 * 修改日期    		修改人员   	版本	 		修改内容<br>
 * -------------------------------------------------<br>
 * 2018/7/24   hui.zhao     1.0    	初始化创建<br>
 * </p>
 *
 * @author hui.zhao
 * @version 1.0
 * @since JDK1.7
 */
public class HttpUtils {


    private static HttpUtils utils = new HttpUtils();

    private final OKHttpUtils okUtils = new OKHttpUtils();

    private HttpUtils() {

    }

    public static HttpUtils getInstance() {
        if (utils == null) {
            synchronized (HttpUtils.class) {
                if (utils == null)
                    utils = new HttpUtils();
            }
        }

        return utils;
    }

    /**
     * get 请求
     *
     * @param url
     * @param params
     * @return
     * @throws InterruptedException
     */
    public Response get(String url, Map<String, String> params) throws Exception {
        HttpUrl httpUrl = new HttpUrl(url);

        if (!Objects.isNull(params)) {
            Set<String> keys = params.keySet();
            keys.forEach(key -> {
                String value = params.get(key);
                httpUrl.addQueryParameter(key, value);
            });
        }

        return okUtils.execute(httpUrl, null);
    }

    /**
     * get 请求
     *
     * @param url
     * @param params
     * @return
     * @throws InterruptedException
     */
    public Response get(String url, final Map<String, String> params, String action) throws Exception {
        HttpUrl httpUrl = new HttpUrl(url);

        if (!Objects.isNull(params)) {
            Set<String> keys = params.keySet();
            keys.stream().forEach(key -> {
                String value = params.get(key);
                httpUrl.addQueryParameter(key, value);
            });
        }

        Request request = new Request.Builder().addHeader("APIName", action).addHeader("CIP", NetUtils.getLocalIp())
                .addHeader("Plevel", 5 + "").addHeader("CName", "dsf.native.httputils").url(httpUrl.build()).build();

        return okUtils.execute(request);
    }

    /**
     * 单独使用的post
     *
     * @param url
     * @param body
     * @return
     * @throws InterruptedException
     */
    public Response post(String url, Map<String, String> params, String body) throws Exception {
        HttpUrl httpUrl = new HttpUrl(url);
        if (!Objects.isNull(params)) {
            Set<String> keys = params.keySet();
            keys.forEach(key -> {
                String value = params.get(key);
                httpUrl.addQueryParameter(key, value);
            });
        }

        return okUtils.execute(httpUrl, body);
    }

    public final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    /**
     * post 到.net接口
     *
     * @param url
     * @param body
     * @return
     * @throws InterruptedException
     */
    public Response post(String url, Map<String, String> params, String body, String action) throws Exception {
        HttpUrl httpUrl = new HttpUrl(url);
        if (!Objects.isNull(params)) {
            Set<String> keys = params.keySet();
            keys.forEach(key -> {
                String value = params.get(key);
                httpUrl.addQueryParameter(key, value);
            });
        }

        Request request = null;
        if (StringUtils.isNotBlank(body)) {
            RequestBody requestBody = RequestBody.create(JSON, body);
            request = new Request.Builder().url(httpUrl.build()).post(requestBody).build();
        } else {
            request = new Request.Builder().url(httpUrl.build()).build();
        }

        request = request.newBuilder().addHeader("APIName", action).addHeader("CIP", NetUtils.getLocalIp())
                .addHeader("Plevel", 5 + "").addHeader("CName", "dsf.native.httputils").url(httpUrl.build()).build();



        return okUtils.execute(request);
    }

}
