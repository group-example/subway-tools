package com.ly.subway.subwaytools.dsfstarter.utils;

import com.ly.spat.dsf.constants.Constants;
import com.ly.spat.dsf.serializer.JsonSerializer;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.codec.http.websocketx.extensions.compression.WebSocketServerCompressionHandler;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.ServerSocket;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * 公共常量类定义
 * <p>
 * <p>
 * 修改历史:											<br>
 * 修改日期    		修改人员   	版本	 		修改内容<br>
 * -------------------------------------------------<br>
 * 2018/7/24   hui.zhao     1.0    	初始化创建<br>
 * </p>
 *
 * @author hui.zhao
 * @version 1.0
 * @since JDK1.7
 */
public class SmokeUtils {
    //   @Parameter(defaultValue = "http://api.dsf.17usoft.com/pub/Package/QueryPackage?envir=test&name=DSF.Smoketool")
    public static final String url="http://api.dsf.17usoft.com/pub/Package/QueryPackage?envir=test&name=DSF.Smoketool";
    public static final int PORT = 16000;

    public static final CountDownLatch latch = new CountDownLatch(1);

    private static final Logger log = LoggerFactory.getLogger(SmokeUtils.class);

    /**
     * classpath
     */
//    @Parameter(defaultValue = "${project.build.outputDirectory}", required = true)
    private String classPath;

    public static void confirmOS() {

        String os = System.getProperty("os.name");
        String DSF_HOME = System.getProperty(Constants.DSFHOME_ENV);

        //只有windows系统可以使用smoketool工具
        if (os.toUpperCase().startsWith("WINDOWS")) {
            log.info("this system is windows which can use the smoketool");
           startSmokeTool();
        }
    }
    
        private static void startSmokeTool ()
        {
            try {
               new Thread(new Runnable() {
                   @Override
                   public void run() {
                       websocketServer();
                   }
               }).start();
                // check version if have a new version--- force update
            //    if (isUpdate) {
             //       checkVersion();
             //   }
                String smoke_env_path = System.getenv("DSFHOME") + "/Smoke";
                Runtime.getRuntime()
                        .exec(smoke_env_path + "/win/dsf.smoketool.win.exe " + " ws://127.0.0.1:" + PORT + "/load");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        /**
         * 检查新版本
         */
        private static void checkVersion ()
        {

            // new Thread(() -> {
            // 拷贝到正在运行的地方
            String DSFHOME = System.getenv("DSFHOME");
            String smoke_env_path = System.getenv("DSFHOME") + "/Smoke";
            String path = smoke_env_path + "/win/version";
            File file = new File(path = path.replace("\\", "/"));

            BufferedReader in = null;
            BufferedInputStream instream = null;
            BufferedOutputStream out = null;
            try {
                in = new BufferedReader(new FileReader(file));
                // 取最新版本
                System.out.println("自动更新smoketool程序......");
                Objects.requireNonNull(SmokeUtils.url, "smoke tool 更新url不能为空");
                String downloadPath = System.getProperty("java.io.tmpdir");

                File downloadFile = new File(downloadPath + "/smoke.zip");

                Response response = HttpUtils.getInstance().post(url, null, null);

                if (Objects.isNull(response) || response.code() != 200) {
                    log.error("更新smoke toole 失败!");
                }

                ResponseUtils utils = new ResponseUtils();
                String result = utils.getResult(response);

                JsonSerializer serializer = new JsonSerializer();

                ToolEntity entity = serializer.serializer(result, ToolEntity.class);
                URL httpurl = new URL(entity.getDownloadUrl());

                InputStream input = httpurl.openStream();
                out = new BufferedOutputStream(new FileOutputStream(downloadFile));
                int i = 0;
                byte[] bs = new byte[1024];
                while ((i = input.read(bs)) != -1) {
                    out.write(bs, 0, i);
                }
                out.flush();
                // 解压
                decompress(downloadPath, downloadFile);

                // 执行拷贝
                String sourceFolder = downloadPath + "/smoke".replaceAll("/", "\\\\");
                String descFolder = DSFHOME.replaceAll("/", "\\\\");
                Runtime.getRuntime().exec("taskkill /F /IM dsf.smoketool.win.exe"); // 关闭后复制
                Runtime.getRuntime().exec("xcopy /c /s /e /f /y " + sourceFolder + " " + descFolder);
                Thread.sleep(10 * 1000);
                System.out.println("更新smoketool完成. 请重新运行程序..");
                System.exit(0);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (in != null) {
                        in.close();
                    }

                    if (instream != null) {
                        instream.close();
                    }

                    if (out != null) {
                        out.close();
                    }
                } catch (IOException e) {
                    in = null;
                }

            }
        }


        /**
         * 解压文件
         *
         * @param downloadPath
         * @param downloadFile
         * @throws FileNotFoundException
         */
        private static void decompress (String downloadPath, File downloadFile) throws FileNotFoundException {
            // 解压文件
            Charset gbk = Charset.forName("gbk");
            ZipInputStream zin = new ZipInputStream(new FileInputStream(downloadFile),gbk);
            BufferedInputStream bin = new BufferedInputStream(zin);

            String smoke_path = downloadPath + "/smoke"; // 输出文件夹
            File fout = null;
            ZipEntry entry;
            try {
                while ((entry = zin.getNextEntry()) != null) {
                    fout = new File(smoke_path, entry.getName());
                    if (entry.isDirectory()) {
                        (new File(fout.getAbsolutePath())).mkdirs();
                        continue;
                    }
                    if (!fout.exists()) {
                        (new File(fout.getParent())).mkdirs();
                    }
                    FileOutputStream fileOut = new FileOutputStream(fout);
                    BufferedOutputStream Bout = new BufferedOutputStream(fileOut);
                    int b;
                    while ((b = bin.read()) != -1) {
                        Bout.write(b);
                    }
                    Bout.close();
                    System.out.println(fout + "解压成功");
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (zin != null) {
                        zin.close();
                    }

                    if (bin != null) {
                        bin.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    public static void websocketServer() {
        try {

            ServerBootstrap b = new ServerBootstrap();
            NioEventLoopGroup bossGroup = new NioEventLoopGroup(1);
            NioEventLoopGroup workerGroup = new NioEventLoopGroup(1);

            b.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {

                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            ChannelPipeline pipeline = ch.pipeline();
                            pipeline.addLast(new HttpServerCodec());
                            pipeline.addLast(new HttpObjectAggregator(65536));
                            pipeline.addLast(new WebSocketServerCompressionHandler());
                            pipeline.addLast(new WebSocketServerProtocolHandler("/load", null, true));
                            pipeline.addLast(new WebSocketFrameHandler());
                        }

                    });

            ServerSocket serverSocket = new ServerSocket(0); // 读取空闲的可用端口

            log.info("websocker port :" + PORT);
            serverSocket.close();

            ChannelFuture future= b.bind(PORT);

            future.addListener(new ChannelFutureListener() {

                @Override
                public void operationComplete(ChannelFuture future) throws Exception {
                    latch.countDown();
                }
            });

            future.sync();
            future.channel().closeFuture().sync();
        } catch (Exception e) {

        }
    }

    }
