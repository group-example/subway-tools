package com.ly.subway.subwaytools.dsfstarter;

import com.ly.subway.subwaytools.dsfstarter.utils.SmokeUtils;
import com.ly.subway.subwaytools.dsfserver.DSF;
import com.ly.subway.subwaytools.dsfserver.Server;
import com.ly.subway.subwaytools.dsfserver.startup.DSFApplicationEnv;
import com.ly.subway.subwaytools.dsfserver.transport.HttpServer;
import com.ly.subway.subwaytools.dsfserver.utils.ClassUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 *
 *
 *
 *
 * <p>
 * 修改历史:                                                                                    &lt;br&gt;
 * 修改日期             修改人员        版本                     修改内容
 * --------------------------------------------------------------------
 * 2018年07月09日 下午2:27   shikai.liu     1.0   初始化创建
 * </p>
 *
 * @author shikai.liu
 * @version 1.0
 * @since JDK1.7
 */

@Configuration
@ComponentScan(basePackages = {"com.ly.spat.dsf","com.ly.subway.tcspringbootstarter"})
public class EnableDsfAutoconfig {

	private static final Logger log = LoggerFactory.getLogger(EnableDsfAutoconfig.class);

	@Autowired
	private DSF dsf;

	@Autowired
	private ApplicationContext applicationContext;


	@Bean
	public ClassUtils classUtils() {
		ClassUtils utils = null;
		//check if the os can use the smoke tool
		try {
			printLogo();
			//初始化dsf的env，包括读取环境变量
			dsf.getEnv("");
			utils = new ClassUtils();
			//读取springboot的fatjar
			utils.springBootStart();
		}catch (Throwable e)
		{
			log.info("class not find,",e.getMessage());
		}
		//启动本地测试工具smokeTool
		SmokeUtils.confirmOS();
		return utils;
	}
	@Bean
	@ConditionalOnBean(ClassUtils.class)
	public String start()
	{

		DSFApplicationEnv.springInit(applicationContext);
		new Thread(new Runnable() {
			@Override public void run() {
				final Server server = new HttpServer(dsf.getConfig(), dsf.getKernel());
				try {
					server.start();
				} catch (Exception e) {
					log.error("server init failed", e);
					if (server != null) {
						server.stop();
					}
				};
			}
		}).start();

		return "";
	}



	private void printLogo() {
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
		}
		System.out.println();
		System.out.println();
		System.out.println("::::::::::         ::::::::::::     :::::::::::::");
		System.out.println("::        ::      ::                ::           ");
		System.out.println("::         ::     ::                ::           ");
		System.out.println("::         ::     :::::::::::::     :::::::::::: ");
		System.out.println("::         ::                ::     ::           ");
		System.out.println("::        ::                 ::     ::           ");
		System.out.println("::::::::::        :::::::::::       ::           ");
		System.out.println();
	}
}
