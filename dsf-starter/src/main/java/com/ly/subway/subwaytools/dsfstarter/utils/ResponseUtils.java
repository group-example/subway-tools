package com.ly.subway.subwaytools.dsfstarter.utils;

import okhttp3.Response;
import okhttp3.ResponseBody;

import java.util.Objects;

/**
 * 公共常量类定义
 * <p>
 * <p>
 * 修改历史:											<br>
 * 修改日期    		修改人员   	版本	 		修改内容<br>
 * -------------------------------------------------<br>
 * 2018/7/24   hui.zhao     1.0    	初始化创建<br>
 * </p>
 *
 * @author hui.zhao
 * @version 1.0
 * @since JDK1.7
 */
public class ResponseUtils {
    /**
     * 取回body 并close
     * @param response
     * @return
     */
    public String getResult(Response response){
        ResponseBody body = response.body();
        try {
            return body.string();
        }catch(Exception e){
            throw new IllegalStateException(e);
        }finally {
            if (!Objects.isNull(body)) {
                body.close();
            }
        }
    }
}
