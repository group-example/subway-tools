package com.ly.subway.subwaytools.dsfserver.transport;

import com.ly.spat.dsf.filter.FilterChain;
import com.ly.subway.subwaytools.dsfserver.handler.HttpHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;

/**
 * Created by lwy32937 on 2016/8/22.
 */
public class HttpInitializer extends ChannelInitializer<SocketChannel> {
	
	private static FilterChain filterChain;
	
//	static final EventExecutorGroup group = new DefaultEventExecutorGroup(16, new DSFThreadFactory(""));

    public HttpInitializer(FilterChain filterChain) {
    	HttpInitializer.filterChain = filterChain;
    }

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
    	ch.pipeline().addLast(new HttpRequestDecoder());
    	ch.pipeline().addLast(new HttpResponseEncoder());
        ch.pipeline().addLast("httpHandler", new HttpHandler(filterChain));
    }
}
