package com.ly.subway.subwaytools.dsfserver.transport;

import com.ly.subway.subwaytools.dsfserver.Server;
import com.ly.subway.subwaytools.dsfserver.invoker.AsyncInvoker;
import com.ly.subway.subwaytools.dsfserver.utils.ServiceUtils;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.WriteBufferWaterMark;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import java.net.BindException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zeromq.ZFrame;
import org.zeromq.ZMQ;
import org.zeromq.ZMsg;

import com.ly.spat.dsf.client.utils.ServerUtils;
import com.ly.spat.dsf.constants.Constants;
import com.ly.spat.dsf.factory.DSFThreadFactory;
import com.ly.spat.dsf.filter.FilterChain;
import com.ly.spat.dsf.kernel.RegistryKernel;
import com.ly.spat.dsf.kernel.ServiceConfig;
import com.ly.spat.dsf.listener.FutureEvent;
import com.ly.spat.dsf.listener.FutureListener;
import com.ly.spat.dsf.utils.CharsetUtils;
import com.ly.spat.dsf.utils.NetUtils;
import com.ly.spat.dsf.utils.StopWatch;
import com.ly.spat.dsf.utils.StringUtils;
import com.ly.spat.dsf.utils.ZmqCache;
import com.ly.spat.dsf.utils.ZmqReqType;

/**
 * server used netty
 * 
 * @author lwy32937
 *
 */
public class HttpServer implements Server {

	private static final Logger logger = LoggerFactory.getLogger(HttpServer.class);

	private static EventLoopGroup bossGroup;

	private static EventLoopGroup workerGroup;

	private final ServerBootstrap b = new ServerBootstrap();

	private final FilterChain filterChain;

	private final StopWatch watch = new StopWatch();

	private final DSFThreadFactory threadFactory = new DSFThreadFactory("dsfstarter-server");

	/** default server port */
	private int port = 16000;

	private ServiceConfig config;

	private RegistryKernel kernel;

	private final ScheduledExecutorService scheduled = Executors.newSingleThreadScheduledExecutor(threadFactory);

	private final CountDownLatch latch = new CountDownLatch(1);

	private volatile static AtomicBoolean FIRST_LOAD = new AtomicBoolean(false);
	
	private ServiceUtils utils = new ServiceUtils();

	public HttpServer(ServiceConfig config, RegistryKernel kernel) {
		this(config.getPort());
		this.config = config;
		this.kernel = kernel;
	}

	public HttpServer(int port) {
		this(port, 0, 0);
	}

	public HttpServer(int port, int bossThreads, int workerThreads) {
		this(port, bossThreads, workerThreads, null);
	}

	public HttpServer(int port, int bossThreads, int workerThreads, FilterChain filterChain) {
		if (port != 0) {
			this.port = port;
		}
		
		bossGroup = new NioEventLoopGroup(1);
		workerGroup = new NioEventLoopGroup(
				workerThreads != 0 ? workerThreads : Runtime.getRuntime().availableProcessors() * 2);
		this.filterChain = (filterChain == null ? FilterChain.getInstance() : filterChain);
	}

	/**
	 * 启动
	 */
	public void start() {
		watch.start();
		int coresize = Runtime.getRuntime().availableProcessors() * 2;
		AsyncInvoker.getInvoker().init(coresize, config.getThreads(), config.getAccepts()); //初始化thread数
		b.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class).option(ChannelOption.SO_BACKLOG, 1000)
				.option(ChannelOption.TCP_NODELAY, true);
		b.childOption(ChannelOption.WRITE_BUFFER_WATER_MARK,new WriteBufferWaterMark(8 * 1024, 32 * 1024)).childHandler(new HttpInitializer(filterChain));

		try {
			doStart();
		} catch (InterruptedException e) {
			logger.error("server start error", e);
		}
	}

	static {
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			// 等待线程池
			try {
				// 若没有执行完， 则循环等待, 10秒循环一次
				if (!Objects.isNull(AsyncInvoker.getInvoker().getService())) {
					AsyncInvoker.getInvoker().getService().shutdown();
					AsyncInvoker.getInvoker().getService().awaitTermination(1000 * 10, TimeUnit.MILLISECONDS);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (!Objects.isNull(bossGroup)) {
				try {
					bossGroup.shutdownGracefully().sync();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			if (!Objects.isNull(workerGroup)) {
				try {
					workerGroup.shutdownGracefully().sync();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			logger.info("DSF服务已经关闭成功");
		}));
	}

	/**
	 * 关闭服务
	 */
	public void stop() {
		// 加入一个关闭的钩子， 用于优雅退出
		logger.warn("netty eventLoop is shutdown");
		bossGroup.shutdownGracefully();
		workerGroup.shutdownGracefully();

	}

	// 启动服务端
	public void doStart() throws InterruptedException {
		try {
			config.setPort(port);
			ChannelFuture future = b.bind(port).sync();
			watch.stop();
			logger.info("server {} is started in {} ms, listening port: {}", config.getGsName(), watch.getTime(), port);
			addListener(future);
			future.channel().closeFuture().sync();
		} catch (Exception e) {
			logger.error("server init error", e);
			if (e instanceof BindException) { // 端口占用
				Runtime.getRuntime().exit(0);
			}
		} finally {
			stop();
		}
	}

	/**
	 * 绑定端口后加入listener
	 * 
	 * @param future
	 */
	private void addListener(ChannelFuture future) {

		/**
		 * 加入接口监听
		 */
		future.addListener(new ChannelFutureListener() {
			@Override
			public void operationComplete(ChannelFuture future) throws Exception {
				if (future.isSuccess()) {
					listener(config); // 监听
				}
			}
		});

		future.addListener(new ChannelFutureListener() {

			@Override
			public void operationComplete(ChannelFuture future) throws Exception {
				try {
					//等等端口绑定成功后运行
					latch.await(10,TimeUnit.SECONDS);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if (config.isKernel()) { // 是kernel服务
					afterKernelStart();
				} else {
					afterStart();
				}
			}

			private void afterKernelStart() {
				utils.execReloading();
				kernel.addListener(new FutureListener() {
					@Override
					public synchronized void actionListener(FutureEvent e) {
						utils.reloadTask(kernel, config);
					}
				});

				if (loadKernel(kernel)) {
					// 发送kernel心跳
					scheduled.scheduleAtFixedRate(() -> {
						kernel.kernelHeart(ZmqReqType.KERNEL_HEART, NetUtils.pickIp(config.getIps()));
					}, Constants.LONG_PREIORD, Constants.LONG_PREIORD, TimeUnit.MILLISECONDS);
				}

			}

			/** 挂载 */
			private boolean loading(final RegistryKernel kernel) {
				// getComponentPath kernel的时候去解析ip
				logger.debug("kernel get center ip start.");
				String[] ips = null;
				try {
					ips = ServerUtils.getHosts(config.getHost());
					config.setIps(ips);
				} catch (Exception e) {
					logger.error("getComponentPath kernel error", e);
				}
				logger.debug("kernel get center ip success.");
				return kernel.loadKernel(ZmqReqType.KERNEL_LOAD, NetUtils.pickIp(config.getIps()), config.getPort()); // 重新挂载
			}

			private boolean loadKernel(final RegistryKernel kernel) {
				// 挂载kernel 先注释掉
				boolean flag = loading(kernel);
				if (!flag) {
					// 重试所有的控制中心
					String[] ips = config.getIps();
					if (ips != null && ips.length > 0) {
						List<String> ipslist = Arrays.asList(ips);
						Map<String, String> tempIpMap = new HashMap<>();
						tempIpMap.putAll(NetUtils.pickIp(config.getIps()));
						for (int i = 0; i < ipslist.size(); i++) {
							flag = kernel.loadKernel(ZmqReqType.KERNEL_LOAD, tempIpMap, config.getPort());
							if (flag) {
								break;
							}
							String newIp = ipslist.get(i);
							tempIpMap.put(Constants.CENTER_IP_KEY, newIp);
							NetUtils.pickIp(config.getIps()).clear();
						}
					}
				}

				if (flag && !FIRST_LOAD.getAndSet(true)) { // 第一次挂载才拉起服务
					utils.lookupService(kernel);
				}

				return flag;
			}

			private void afterStart() {
				utils.register(config);
				utils.checkRegistry();
			}
		});

	}

	/**
	 * 
	 * 启动守护线程用于接收stop虚拟机命令
	 *
	 *            //启动服务的时候的名字
	 */
	public void listener(ServiceConfig config) {
		Thread t = threadFactory.newThread(() -> {
			ZMQ.Context context = ZMQ.context(1);
			// Socket to talk to clients
			ZMQ.Socket responder = context.socket(ZMQ.REP);
			responder.setIdentity((config.getPort() + "").getBytes());
			try {
				StringBuilder sb = new StringBuilder();
				sb.append("ipc://").append(config.getPort()).append("/").append(config.getFolderName()).append("/server/0");
				responder.bind(sb.toString());
				logger.debug("binding a server to exec comman,bind to:{}",	sb.toString());

				if(!config.isKernel()) {
					ZmqCache.writeProperties(config.getGsName(), config.getPort());
					//写入properties
					logger.info("写入zmqperoperties: folerName:{}, port:{}", config.getGsName(), config.getPort());
				}

				latch.countDown();
				
				while (!Thread.currentThread().isInterrupted()) {
					ZMsg msg = ZMsg.recvMsg(responder);
					String command = getMsg(msg.getFirst());
					if (StringUtils.isBlank(command))
						return;

					if (command.equals("stop")) {
						System.exit(0); // 自动会调用stop
					} else if (command.equals("test")) {
						responder.send("ok".getBytes());
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(0);
			} finally {
				responder.close();
				context.term();
			}
		});

		t.start();
	}

	private String getMsg(ZFrame frame) {
		byte[] bs = frame.getData();
		return new String(bs, CharsetUtils.UTF_8);
	}

}
