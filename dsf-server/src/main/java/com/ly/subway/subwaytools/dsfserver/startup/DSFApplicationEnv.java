package com.ly.subway.subwaytools.dsfserver.startup;

import com.ly.spat.dsf.exception.DSFInitException;
import com.ly.spat.dsf.kernel.ServiceConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * spring bean scanner
 * @author lwy32937
 *
 */
public class DSFApplicationEnv {
	
	private final static AtomicBoolean isInit = new AtomicBoolean(false);
	
	private static AnnotationConfigApplicationContext context;
	private static ApplicationContext applicationContext;

	private Object lock = new Object();
	
	private static volatile DSFApplicationEnv config = new DSFApplicationEnv();
	
	private volatile static List<Class<?>> list = new CopyOnWriteArrayList<>();
	
	private DSFApplicationEnv(){}
	
	public static List<Class<?>> getList() {
		return list;
	}

	public static void setList(List<Class<?>> list) {
		DSFApplicationEnv.list = list;
	}

	public static DSFApplicationEnv getConfig(){
		if(Objects.isNull(config)){
			synchronized (DSFApplicationEnv.class) {
				if(Objects.isNull(config)){
					config = new DSFApplicationEnv();
				}
			}
		}
		return config;
	}

	public void init(String ...scanPath) {
		/**only init once time*/
		synchronized (lock) {
			if(!isInit.getAndSet(true)) {
				context = new AnnotationConfigApplicationContext();
				context.scan(scanPath);
				context.refresh();
			}
		}
	}
	
	public void init(Class<?>... clazz) {
		/**only init once time*/
		synchronized (lock) {
			if(!isInit.getAndSet(true)) {
				if(!Objects.isNull(context)) {
					context.register(clazz);
					context.refresh();
				}else {
					context = new AnnotationConfigApplicationContext(clazz);
				}
			}
		}
	}
	
	/**
	 * 同步防止对synchronized做修改的时候，别人读取，导致死锁
	 * @return
	 */
	public static synchronized ApplicationContext getContext() {
		if(!isInit.get()) throw new DSFInitException("spring scaner have not been finished");
		if(context==null) {
			return applicationContext;
		}
		return context;
	}
	
	public static ServiceConfig getServiceConfig() {
		if(!isInit.get()) throw new DSFInitException("可能缺少服务必须的基础配置");	
		synchronized (DSFApplicationEnv.class) {
			return context.getBean(ServiceConfig.class);
		}
	}

	/**
	 * 初始化springboot的环境
	 * @param context1
	 */
	public static void springInit(ApplicationContext context1)
	{
		if(!isInit.getAndSet(true))
		{
			applicationContext=context1;
		}
	}
}
