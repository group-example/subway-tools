package com.ly.subway.subwaytools.dsfserver.context;

import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLStreamHandlerFactory;
import java.util.jar.Manifest;

/**
 * jsf classloader
 * @author lwy32937
 * @2016年6月27日 上午9:12:29
 */
public class DSFClassLoader extends URLClassLoader {

	public DSFClassLoader(URL[] urls, ClassLoader parent, URLStreamHandlerFactory factory) {
		super(urls, parent, factory);
	}

	public DSFClassLoader(URL[] urls, ClassLoader parent) {
		super(urls, parent);
	}

	public DSFClassLoader(URL[] urls) {
		super(urls);
	}

	@Override
	public URL[] getURLs() {
		return super.getURLs();
	}

	@Override
	protected Class<?> findClass(String name) throws ClassNotFoundException {
		return super.findClass(name);
	}

	@Override
	protected Package definePackage(String name, Manifest man, URL url) throws IllegalArgumentException {
		return super.definePackage(name, man, url);
	}

}
