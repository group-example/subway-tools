package com.ly.subway.subwaytools.dsfserver.monitor;

import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.lang.management.RuntimeMXBean;

/**
 * 通过jmx端口取cpu使用率
 */
public class JVMCPUUsage {
    // hardcoded connection parameters
    private final String HOSTNAME = "localhost";
    private final int PORT;

    private MBeanServerConnection mBeanServerConnection;
    private com.sun.management.OperatingSystemMXBean peOperatingSystemMXBean;
    private OperatingSystemMXBean operatingSystemMXBean;
    private RuntimeMXBean runtimeMXBean;

    // keeping previous timestamps
    private long previousJvmProcessCpuTime = 0;
    private long previousJvmUptime = 0;

    private JVMCPUUsage(Builder builder) {
        this.PORT = builder.port;
    }

    public static class Builder {
        private final int port;

        public Builder(int port){
            this.port = port;
        }

//        public JVMCPUUsage build() {
//            return new JVMCPUUsage(this).openMBeanServerConnection().getMXBeanProxyConnections();
//        }
    }

    // initiate and prepare MBeanServerConnection
    public JVMCPUUsage openMBeanServerConnection(){
        try {
            // initiate address of the JMX API connector server
            String serviceURL = "service:jmx:rmi:///jndi/rmi://" + HOSTNAME + ":" + PORT + "/jmxrmi";
            JMXServiceURL jmxServiceURL = new JMXServiceURL(serviceURL);

            // initiate client side JMX API connector
            // here we set environment attributes to null, because it is not a necessity to what we're going to do
            JMXConnector jmxConnector = JMXConnectorFactory.connect(jmxServiceURL, null);

            // initiate management bean server connection
            mBeanServerConnection = jmxConnector.getMBeanServerConnection();
            return this;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    // initiate and prepare MXBean interfaces proxy connections
    public JVMCPUUsage getMXBeanProxyConnections() {
        try {
            peOperatingSystemMXBean = ManagementFactory.newPlatformMXBeanProxy(
                    mBeanServerConnection,
                    ManagementFactory.OPERATING_SYSTEM_MXBEAN_NAME,
                    com.sun.management.OperatingSystemMXBean.class
            );
            operatingSystemMXBean = ManagementFactory.newPlatformMXBeanProxy(
                    mBeanServerConnection,
                    ManagementFactory.OPERATING_SYSTEM_MXBEAN_NAME,
                    OperatingSystemMXBean.class
            );
            runtimeMXBean = ManagementFactory.newPlatformMXBeanProxy(
                    mBeanServerConnection,
                    ManagementFactory.RUNTIME_MXBEAN_NAME,
                    RuntimeMXBean.class
            );

            return this;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    // Get JVM CPU usage
    public float getJvmCpuUsage() {
        // elapsed process time is in nanoseconds
        long elapsedProcessCpuTime = peOperatingSystemMXBean.getProcessCpuTime() - previousJvmProcessCpuTime;
        // elapsed uptime is in milliseconds
        long elapsedJvmUptime = runtimeMXBean.getUptime() - previousJvmUptime;

        // total jvm uptime on all the available processors
        long totalElapsedJvmUptime = elapsedJvmUptime * operatingSystemMXBean.getAvailableProcessors();

        // calculate cpu usage as a percentage value
        // to convert nanoseconds to milliseconds divide it by 1000000 and to get a percentage multiply it by 100
        float cpuUsage = elapsedProcessCpuTime / (totalElapsedJvmUptime * 10000F);

        // set old timestamp values
        previousJvmProcessCpuTime = peOperatingSystemMXBean.getProcessCpuTime();
        previousJvmUptime = runtimeMXBean.getUptime();

        return cpuUsage;
    }
}