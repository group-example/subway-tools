package com.ly.subway.subwaytools.dsfserver.utils;

import com.ly.spat.dsf.filter.DefaultDsfFilter;
import com.ly.spat.dsf.filter.FilterChain;
import com.ly.spat.dsf.kernel.ServiceConfig;
import com.ly.spat.dsf.utils.FileUtils;
import com.ly.spat.dsf.utils.ObjectHelper;
import com.ly.spat.dsf.utils.StopWatch;
import com.ly.spat.dsf.utils.StringUtils;
import com.ly.subway.subwaytools.dsfserver.startup.DSFApplicationEnv;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * 加载DSF service
 *
 * @author lwy32937
 * @2016年6月22日 下午4:38:36
 */
public class ClassUtils {

    private static final Logger logger = LoggerFactory.getLogger(ClassUtils.class);

    private final StopWatch watch = new StopWatch();

    private final ServiceConfig config; // 用于向注册中心注册服务

    private final String pth = "com.ly.spat.dsf.kernel.service";

    public ClassUtils() {
        this(null);
    }

    public ClassUtils(ServiceConfig config) {
        this.config = config;
    }

    private void loadBean(String path, String scanPath) {
        try {
            FilterChain.getInstance().addFilter(new DefaultDsfFilter()); //初始化filter
            File file;
            ClassLoader loader = Thread.currentThread().getContextClassLoader();
            Enumeration<URL> enums = loader.getResources(path);
            while (enums.hasMoreElements()) {
                URL url = enums.nextElement();
                String protocol = url.getProtocol();
                if ("file".equals(protocol)) {
                    String filePath = URLDecoder.decode(url.getFile(), "UTF-8");
                    file = new File(filePath);
                    loadAnnotation(file, scanPath);
                }

                if ("jar".equals(protocol)) {
                    logger.debug("start getComponentPath dependency jar path:{}", scanPath);
                    String filePath = getFilePath(url.getPath());
                    logger.debug("file path:{}", filePath);
                    JarFile jarFile = new JarFile(new URL(filePath).getFile());
                    Enumeration<JarEntry> enus = jarFile.entries();
                    while (enus.hasMoreElements()) {
                        JarEntry entry = enus.nextElement();
                        if (entry.getName().endsWith(".class")) {
                            logger.debug("loading class: {}", entry.getName());
                            String fullName = getFullName(entry.getName());
                            DSFLoader.load(loader, fullName);
                        }
                    }
                    jarFile.close();
                }
            }
        } catch (Exception e) {
            throw new IllegalStateException("load clazz error", e);
        }

    }

    /**
     * 加载类 中间会初始化spring
     *
     * @throws IOException
     * @throws UnsupportedEncodingException
     * @throws Exception
     * @throws MalformedURLException
     */
    public void loadBean() throws Exception {
        File file;
        Enumeration<URL> enums = Thread.currentThread().getContextClassLoader().getResources("");
        while (enums.hasMoreElements()) {
            URL url = enums.nextElement();
            String protocol = url.getProtocol();
            if ("file".equals(protocol)) {
                String filePath = URLDecoder.decode(url.getFile(), "UTF-8");
                file = new File(filePath);
                loadAnnotation(file, "");
            }
        }

        List<String> paths = DSFLoader.getPaths();
        paths.stream().forEach(scanpath ->{
            String path = scanpath.replace(".", "/");
            loadBean(path, scanpath);
        });

        List<Class<?>> clazzs = DSFApplicationEnv.getList();
        DSFApplicationEnv.getConfig().init(clazzs.toArray(new Class[0])); // spring init bean
    }


    private String getFullName(String value) {
        return value = (value.replace("/", ".").substring(0, value.length() - 6));
    }

    /**
     * 从jar中截取
     *
     * @param path
     */
    private String getFilePath(String path) {
        int i = path.indexOf("!");
        return path.substring(0, i);
    }

    private void loadAnnotation(File file, String scanPath) {
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File f : files) {
                if (f.isDirectory()) {
                    loadAnnotation(f, StringUtils.isBlank(scanPath) ? f.getName() : scanPath + "." + f.getName());
                } else {
                    loadAnnotation(f, scanPath);
                }
            }
        }

        if (file.isFile()) {
            if (file.getName().endsWith(".class")) {
                // 取出full class name
                String fullClsName = FileUtils.getInstance().getFullClsName(file.getAbsolutePath());
                DSFLoader.load(Thread.currentThread().getContextClassLoader(), scanPath + "." + fullClsName);
            }
        }
    }

	public void springBootStart() {
		try {
			File file;
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			Enumeration<URL> enums = classLoader.getResources("");
			while (enums.hasMoreElements()) {
				URL url = enums.nextElement();
				String protocol = url.getProtocol();
				if ("file".equals(protocol)) {
					String filePath = URLDecoder.decode(url.getFile(), "UTF-8");
					file = new File(filePath);
					loadAnnotationForSpring(file, "", classLoader);
				}
				//只扫描spring中得jar
				else if ("jar".equals(protocol)&&url.getPath().contains("classes!")) {
					String filePath = getFilePath(url.getPath());
					logger.info("start loadKernel dependency jar path:{}", filePath);
					JarFile jarFile = new JarFile(new URL(filePath).getFile());
					Enumeration<JarEntry> enus = jarFile.entries();
					while (enus.hasMoreElements()) {
						JarEntry entry = enus.nextElement();
						if (entry.getName().startsWith( "BOOT-INF/classes")&&entry.getName().endsWith("class")) {
							logger.info("entry is:"+entry.getName());
							String className = entry.getName().replace("BOOT-INF/classes/", "");
							String fullName = getFullName(className);
							logger.info("fullname is:"+fullName);
							DSFLoader.loadClassFromRoot(fullName, classLoader);
						}
					}
					jarFile.close();
				}
			}

		} catch (Exception e) {
			throw new IllegalStateException(e);
		}
	}

	private void loadAnnotation(File file, String scanPath, ClassLoader classLoader) throws Exception {
		if (file.isDirectory()) {
			File[] files = file.listFiles();
			for (File f : files) {
				if (f.isDirectory()) {
					loadAnnotation(f, ObjectHelper.nullOrEmpty(scanPath) ? f.getName() : scanPath + "." + f.getName(), classLoader);
				} else {
					loadAnnotation(f, scanPath, classLoader);
				}
			}
		}
		if (file.isFile()) {
			if (file.getName().endsWith(".class")) {
				// 取出full class name
				String fullClsName = FileUtils.getInstance().getFullClsName(file.getAbsolutePath());
				DSFLoader.load( classLoader,scanPath + "." + fullClsName);
			}
		}
	}

	private void loadAnnotationForSpring(File file, String scanPath, ClassLoader classLoader) throws Exception {
		if (file.isDirectory()) {
			File[] files = file.listFiles();
			for (File f : files) {
				if (f.isDirectory()) {
					loadAnnotationForSpring(f, ObjectHelper.nullOrEmpty(scanPath) ? f.getName() : scanPath + "." + f.getName(),
							classLoader);
				} else {
					loadAnnotationForSpring(f, scanPath, classLoader);
				}
			}
		}
		if (file.isFile()) {
			if (file.getName().endsWith(".class")) {
				// 取出full class name
				String fullClsName = FileUtils.getInstance().getFullClsName(file.getAbsolutePath());
				DSFLoader.loadClassFromRoot(scanPath + "." + fullClsName, classLoader);
			}
		}
	}

}
