package com.ly.subway.subwaytools.dsfserver.protocol;

import io.netty.handler.codec.http.DefaultHttpRequest;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpVersion;

/**
 * 
 * @author lwy32937
 * @2016年6月28日 下午4:41:04
 */
public class HttpRequest extends DefaultHttpRequest {

	public HttpRequest(HttpVersion httpVersion, HttpMethod method, String uri, boolean validateHeaders) {
		super(httpVersion, method, uri, validateHeaders);
	}

	public HttpRequest(HttpVersion httpVersion, HttpMethod method, String uri, HttpHeaders headers) {
		super(httpVersion, method, uri, headers);
	}

	public HttpRequest(HttpVersion httpVersion, HttpMethod method, String uri) {
		super(httpVersion, method, uri);
	}

	
}
