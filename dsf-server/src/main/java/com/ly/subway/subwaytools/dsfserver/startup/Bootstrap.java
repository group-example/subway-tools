package com.ly.subway.subwaytools.dsfserver.startup;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import com.ly.spat.dsf.DSFApplication;
import org.zeromq.ZFrame;
import org.zeromq.ZMQ;
import org.zeromq.ZMsg;

import com.ly.spat.dsf.constants.Constants;
import com.ly.spat.dsf.utils.StringUtils;
import com.ly.spat.dsf.utils.ZipUtils;

/**
 * server dsf入口
 * 
 * @author lwy32937
 * @2016年7月6日 下午4:09:30
 */
public class Bootstrap {
	
	private final static CountDownLatch latch = new CountDownLatch(1);

	public static void main(String[] args) {
		String serviceName = args[0];

		if (args.length <= 1) { // 有两个参数1、service 2、action(启动， 停止。。。。)
			System.out.println("dsfstarter.service.name is null or action is null");
			return;
		}

		// 停止server
		if (StringUtils.isNotBlank(args[1]) && args[1].equals("stop")) {
			try {
				stop(serviceName);
				latch.await(5, TimeUnit.SECONDS); //最多等5秒
				System.out.println("stop " + serviceName + " finished.");
			}catch(Exception e){
				;
			}finally {
				System.exit(0);
			}
			return;
		}

		// 启动
		start(args, serviceName);
	}

	private static void start(String[] args, String serviceName) {
		if (StringUtils.isNotBlank(args[1]) && args[1].equals("start")) {
			// 加载配置
			String user_dir = System.getProperty("user.dir"); // 当前绝对路径
			System.out.println("workspace dir :" + user_dir);
			File file = new File(user_dir);
			boolean flag = false;
			if (file.exists()) {
				File[] files = file.listFiles();
				for (File f : files) {
					if (f.getName().equals("deploy")) {
						flag = true;
						break;
					}
				}
			}
			String path = null;
			if (flag) {// 本地代码
				path = user_dir;
			}else {
				path = System.getProperty(Constants.DSFHOME_ENV);
			}

			String serviceDir = path + "/deploy/" + serviceName;
			File serviceFolder = new File(serviceDir);
			if (!serviceFolder.exists()) {
				// 在deploy里解压zip文件
				File zipFile = new File(path + "/dsfapps/" + serviceName + ".zip");
				if (!zipFile.exists()) {
					stop(serviceName);
					System.out.println(serviceName + " is not exists! pleck check the input serviceName.");
					return;
				}

				// decompress zip
				try {
					ZipUtils.decompress(serviceDir, zipFile);
				} catch (FileNotFoundException e) {
					System.out.println("解压文件失败.");
					e.printStackTrace();
				}
			}

			// 存在服务文件夹
			Global global = new Global();
			try {
				// 默认
				global.addSystemFolder(path + "/extlib");
				global.addSystemFolder(serviceDir + "/lib");
				global.addSystemFolder(serviceDir);

			} catch (Exception e) {
				e.printStackTrace();
			}

			// 启动
			DSFApplication.run(serviceName);
		}
	}

	/**
	 * 停机
	 */
	private static void stop(final String serviceName) {
		new Thread(() -> {
			try {
				ZMQ.Context context = ZMQ.context(1);
				//get zmq port
//				String port = readzmqConfig(serviceName);
				ZMQ.Socket requester = context.socket(ZMQ.REQ);
				requester.connect("ipc://" + serviceName.toLowerCase() + "/server/0");
				String request = "stop";
				ZMsg out = new ZMsg();
				ZFrame frame = new ZFrame(request);
				out.add(frame);
				requester.setSendTimeOut(Constants.LONG_PREIORD);

				out.send(requester);
				requester.close();
				context.term();
			}catch(Exception e) {
				e.printStackTrace();
			} finally {
				latch.countDown();
			}
			
		}).start();;
	}

	/**
	 * 不做写文件
	 * @param name
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	@Deprecated
	private static String readzmqConfig(String name) throws FileNotFoundException, IOException {
		String path = System.getProperty(Constants.DSFHOME_ENV);
		InputStream in = new FileInputStream(new File(path + "/config/zmq.properties"));
		Properties p = new Properties();
		p.load(in);
		String port = p.getProperty(name);
		return port;
	}
}
