package com.ly.subway.subwaytools.dsfserver.handler;

import com.ly.spat.dsf.handler.Handler;

/**
 * 构造链式handler
 * @author lwy32937
 * @2016年6月30日 下午4:14:01
 */
public class Chains {

	public static Handler build() {
		return new ServiceHandler(new InvokerHandler()); 
	}
}
