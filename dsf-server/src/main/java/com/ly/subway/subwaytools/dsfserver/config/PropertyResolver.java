package com.ly.subway.subwaytools.dsfserver.config;

/**
 * 读取properties
 * @author lwy32937 2017年2月7日
 *
 */
public interface PropertyResolver {

	public Object getEnv(String key, Class<?> clazz);
}
