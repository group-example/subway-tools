package com.ly.subway.subwaytools.dsfserver.protocol;


import com.ly.subway.subwaytools.dsfserver.type.PlatformType;
import com.ly.subway.subwaytools.dsfserver.type.SerializerType;

/**
 * jsf protocol
 * @author Service Platform Team (spt@ly.com)
 *
 */
public class DSFProtocol implements Protocol{

	private byte version ; //版本类型
	
	private PlatformType platType; //平台类型
	
	private int length; //数据长度
	
	private SerializerType serializerType; //序列化类型

	public byte getVersion() {
		return version;
	}

	public void setVersion(byte version) {
		this.version = version;
	}

	public PlatformType getPlatType() {
		return platType;
	}

	public void setPlatType(PlatformType platType) {
		this.platType = platType;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public SerializerType getSerializerType() {
		return serializerType;
	}

	public void setSerializerType(SerializerType serializerType) {
		this.serializerType = serializerType;
	}
	
	
}
