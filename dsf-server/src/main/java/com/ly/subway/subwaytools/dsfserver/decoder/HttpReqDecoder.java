package com.ly.subway.subwaytools.dsfserver.decoder;


import com.ly.subway.subwaytools.dsfserver.protocol.HttpRequest;
import io.netty.handler.codec.http.HttpMessage;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpObjectDecoder;
import io.netty.handler.codec.http.HttpVersion;

/**
 * 
 * @author lwy32937
 * @2016年6月28日 下午4:42:37
 */
public class HttpReqDecoder extends HttpObjectDecoder {

	public HttpReqDecoder() {
	}

	/**
	 * Creates a new instance with the specified parameters.
	 */
	public HttpReqDecoder(int maxInitialLineLength, int maxHeaderSize, int maxChunkSize) {
		super(maxInitialLineLength, maxHeaderSize, maxChunkSize, true);
	}

	public HttpReqDecoder(int maxInitialLineLength, int maxHeaderSize, int maxChunkSize, boolean validateHeaders) {
		super(maxInitialLineLength, maxHeaderSize, maxChunkSize, true, validateHeaders);
	}

	public HttpReqDecoder(int maxInitialLineLength, int maxHeaderSize, int maxChunkSize, boolean validateHeaders, int initialBufferSize) {
		super(maxInitialLineLength, maxHeaderSize, maxChunkSize, true, validateHeaders, initialBufferSize);
	}

	@Override
	protected boolean isDecodingRequest() {
		return true;
	}

	@Override
	protected HttpMessage createMessage(String[] initialLine) throws Exception {
		return new HttpRequest(HttpVersion.valueOf(initialLine[2]), HttpMethod.valueOf(initialLine[0]), initialLine[1], validateHeaders);
	}

	@Override
	protected HttpMessage createInvalidMessage() {
		return new HttpRequest(HttpVersion.HTTP_1_0, HttpMethod.GET, "/bad-request", validateHeaders);
	}

}
