package com.ly.subway.subwaytools.dsfserver;

/**
 * interface 
 * @author lwy32937
 *
 */
public interface Server {

	public void start() ;
	
	public void stop() ;
}
