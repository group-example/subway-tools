package com.ly.subway.subwaytools.dsfserver.utils;

import com.codahale.metrics.Timer;
import com.ly.spat.dsf.constants.Constants;
import com.ly.spat.dsf.entity.DSFResponse;
import com.ly.spat.dsf.entity.MetricsKey;
import com.ly.spat.dsf.info.DSFStatus;
import com.ly.spat.dsf.info.Register;
import com.ly.spat.dsf.kernel.ServiceConfig;
import com.ly.spat.dsf.serializer.JsonSerializer;
import com.ly.spat.dsf.serializer.Serializer;
import com.ly.spat.dsf.utils.NetUtils;
import com.ly.spat.dsf.utils.StatusUtils;
import com.ly.subway.subwaytools.dsfserver.handler.InvokerHandler;
import com.ly.subway.subwaytools.dsfserver.invoker.AsyncInvoker;
import com.ly.subway.subwaytools.dsfserver.metrics.ServiceEndpoint;
import com.ly.subway.subwaytools.dsfserver.metrics.ServiceState;
import com.ly.subway.subwaytools.dsfserver.metrics.ServiceStateWrapper;
import com.ly.subway.subwaytools.dsfserver.monitor.DsfMonitor;
import com.ly.subway.subwaytools.dsfserver.monitor.MonitorBean;
import com.ly.subway.subwaytools.dsfserver.startup.DSFApplicationEnv;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Supplier;

/**
 * 数据上传
 *
 * @author lwy32937 2016年9月6日 下午2:23:34
 */
public class UploadStates {

    private static final Logger logger = LoggerFactory.getLogger(UploadStates.class);

    private final static ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();

    private static final Serializer serializer = new JsonSerializer();

    private final Register register;

    private final static AtomicInteger count = new AtomicInteger(0);

    private ServiceConfig config;

    public UploadStates(ServiceConfig config) {
        this.config = config;
        Register param = null;
        try {
            ApplicationContext applicationContext = DSFApplicationEnv.getContext();
            param = applicationContext.getBean(Register.class);

        } catch (Throwable e) {
            logger.info("UploadStates  register is springboot register");
            param = config.getRegister();
        }
        this.register = param;
    }

    DsfMonitor monitor = new DsfMonitor();

    /**
     * 服务状态上传
     */
    public void uploadStates() {

        service.scheduleAtFixedRate(() -> {
            try {
                if (StatusUtils.getIsserving().get()) { // 提供服务的时候才发送心跳
                    logger.debug("正在提供服务，发送心跳");
                    ServiceStateWrapper wrapper = buildWrapper();
                    uploadService(() -> serializer.toJson(wrapper));
                } else {
                    logger.debug("不提供服务了");
                }
            } catch (Exception e) {
                logger.error("build heart data failed", e);
            }

        }, Constants.LONG_PREIORD, Constants.LONG_PREIORD, TimeUnit.MILLISECONDS);
    }

    /**
     * 向注册中心挂载服务 并发送服务信息
     *
     * @param status
     */
    public void uploadService(Supplier<String> status) {
        logger.debug("sending service heart data {}", status.get());
        try {
            DSFResponse response = register.uploadStatus(status);
            if (response.getStatus() != 200) {
                logger.error("getComponentPath service states response from kernel :{}", response.getMessage());
                if (count.getAndIncrement() >= 20) {
                    StatusUtils.isRegistered().set(false); // 设置为false，需要重新挂载服务组
                }
                ;
            }
            ServiceConfig serviceConfig = null;
            try {
                ApplicationContext applicationContext = DSFApplicationEnv.getContext();
                serviceConfig = applicationContext.getBean(ServiceConfig.class);
            } catch (Exception e) {
                serviceConfig = this.config;

            }
            DSFStatus configs = new DSFStatus(config.getGsName(), config.getVersion(),
                    serviceConfig.getStatus().getType(), config.getPlevel() + "", config.getClevel() + "");
            configs.setHost(NetUtils.getLocalIp());
            configs.setPort(config.getPort());
            register.uploadServiceInfo(configs);
        } catch (Exception e) {
            logger.error("send states data to control center failed", e);
        }
    }

    private ServiceStateWrapper buildWrapper() {
        MonitorBean bean = monitor.getMonitorBean(AsyncInvoker.getInvoker().getService());
        ServiceStateWrapper wrapper = new ServiceStateWrapper();
        List<ServiceState> groups = new ArrayList<>();
        wrapper.setGroups(groups);
        try {
            wrapper.setHostIP(NetUtils.getLocalIp());
            wrapper.setUsedCPU(bean.getCpuUsed());
            wrapper.setUsedMemory(bean.getMemeryUsed());
        } catch (Exception e) {
            logger.error("获取cpu、内存数据异常。非持续报该错误，请忽略。异常信息为:", e);
        }

        ServiceState serviceState = new ServiceState();
        List<ServiceEndpoint> endpoints = new ArrayList<>();
        serviceState.setEndpoints(endpoints);
        groups.add(serviceState);

        serviceState.setGSName(config.getGsName());
        serviceState.setCLevel(config.getClevel());
        serviceState.setVersion(config.getVersion());
        serviceState.setUsedMemory(bean.getMemeryUsed());
        serviceState.setUsedCPU(bean.getCpuUsed());
        serviceState.setThreadCount(bean.getThreads()); // 活动线程数

        ServiceEndpoint point = new ServiceEndpoint();
        endpoints.add(point);
        List<ServiceEndpoint.State> states = new ArrayList<>();
        point.setStates(states);
        try {
            point.setEndpoint(NetUtils.getLocalIp() + ":" + config.getPort());
            point.setProcessingRequests(new Double(AsyncInvoker.getInvoker().getService().getActiveCount()).intValue());
        } catch (Exception e) {
            // ignore
        }

        Map<MetricsKey, Timer> timerMaps = InvokerHandler.getTimermaps();
        Map<MetricsKey, AtomicLong> succCounters = InvokerHandler.getSuccesscounters();
        Map<MetricsKey, AtomicLong> failCounters = InvokerHandler.getFailcounters();
        Set<MetricsKey> set = timerMaps.keySet();
        Iterator<MetricsKey> it = set.iterator();
        while (it.hasNext()) {
            ServiceEndpoint.State stat = new ServiceEndpoint.State();
            states.add(stat);
            MetricsKey key = it.next();
            Timer timer = timerMaps.get(key);
            AtomicLong succ = succCounters.get(key);
            AtomicLong fail = failCounters.get(key);

            //转为小写
            stat.setActionName(key.getMethodName().toLowerCase());
            stat.setSName(key.getsName());

            ServiceEndpoint.Consumers consumer = new ServiceEndpoint.Consumers();
            List<ServiceEndpoint.Consumers> consumerList = new ArrayList<>();
            stat.setConsumers(consumerList);
            consumerList.add(consumer);
            consumer.setAvg(timer.getSnapshot().getMedian() / 1000000);
            consumer.setPercentile99(timer.getSnapshot().get99thPercentile());
            consumer.setPercentile98(timer.getSnapshot().get98thPercentile());
            consumer.setPercentile95(timer.getSnapshot().get95thPercentile());
            consumer.setPercentile75(timer.getSnapshot().get75thPercentile());
            consumer.setMax(timer.getSnapshot().getMax() / 1000000);
            consumer.setMin(timer.getSnapshot().getMin() / 1000000);

            if (!Objects.isNull(fail)) {
                consumer.setCount(succ.getAndSet(0));
            }

            if (!Objects.isNull(fail)) {
                consumer.setFail(fail.getAndSet(0));
            }
            consumer.setIP(key.getChost());
            consumer.setName(key.getcName());
            InvokerHandler.getTimermaps().remove(key);
        }

        return wrapper;

    }


}
