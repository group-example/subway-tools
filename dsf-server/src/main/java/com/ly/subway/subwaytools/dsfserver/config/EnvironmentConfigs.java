package com.ly.subway.subwaytools.dsfserver.config;

import java.util.Objects;

/**
 * server config
 * @author lwy32937 2017年2月7日
 *
 */
public class EnvironmentConfigs {
	
	private static EnvironmentConfigs configs;
	
	private PropertyResolver resolver ;
	
	private EnvironmentConfigs() {}
	
	private EnvironmentConfigs(PropertyResolver resolver) {
		this.resolver = resolver;
	}
	
	/**
	 * 初始化使用
	 * @param resolver
	 * @return
	 */
	public static EnvironmentConfigs getCongigs (PropertyResolver resolver) {
		if(Objects.isNull(configs)) {
			synchronized (EnvironmentConfigs.class) {
				if(Objects.isNull(configs)) {
					configs = new EnvironmentConfigs(resolver);
				}
			}
		}
		
		return configs;
	}
	
	/**
	 * 后边使用
	 * @return
	 */
	public static EnvironmentConfigs getCongigs () {
		return configs;
	}
	
	@SuppressWarnings("unchecked")
	public <T> T getEnv(String key, Class<T> T) {
		return (T) resolver.getEnv(key, T);
	}
}
