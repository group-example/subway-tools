package com.ly.subway.subwaytools.dsfserver.handler;

import com.ly.subway.subwaytools.dsfserver.protocol.HttpProtocol;
import com.ly.subway.subwaytools.dsfserver.protocol.Protocol;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * 需要http decoder encoder支撑
 * @author lwy32937
 * 2016年9月5日 上午10:14:49
 */
public class HttpHandlers extends SimpleChannelInboundHandler<Protocol> {

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, Protocol msg) throws Exception {
		if(msg instanceof HttpProtocol) {
			HttpProtocol message = (HttpProtocol) msg; 
			
		}
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
	}
	
}
