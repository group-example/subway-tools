package com.ly.subway.subwaytools.dsfserver.config;

import java.util.Properties;

/**
 * 默认解析
 * @author lwy32937 2017年2月7日
 *
 */
public class DSFPropertyResolver implements PropertyResolver {

	private final Properties properties = new Properties();
	
	public DSFPropertyResolver(Properties p) {
		p.forEach((key, value) ->{
			properties.setProperty((String)key, (String)value);
		});
	}
	
	@Override
	public Object getEnv(String key, Class<?> clazz) {
		String type = clazz.getTypeName();
		String res = properties.getProperty(key);
		if(type.equals("int")) {
			return res == null ? 0 : Integer.parseInt(res);
		}
		
		if(type.equals("boolean")){
			return res == null ? false : Boolean.parseBoolean(res);
		}
		
		if(type.equals("float")){
			return res == null ? 0.0 : Float.parseFloat(res);
		}
		
		if(type.equals("double")){
			return res == null ? 0.0 : Double.parseDouble(res);
		}
		
		
		return res; 
	}
	

}
