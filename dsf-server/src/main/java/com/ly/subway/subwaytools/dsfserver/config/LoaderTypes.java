package com.ly.subway.subwaytools.dsfserver.config;

/**
 * 区分类
 * @author lwy32937 2017年2月7日
 *
 */
public enum LoaderTypes {

	SC("ServiceContract", 1),
	
	CONFIG("DSFConfigs", 3),
	
	READER("PropertyReader", 2);
	
	private int type;
	
	LoaderTypes(String name, int type) {
		this.type = type;
	}
	
	public static int valueof(String name) {
		
		if(name.equals("ServiceContract")) {
			return SC.type;
		}
		
		if(name.equals("DSFConfigs")) {
			return CONFIG.type;
		}
		
		if(name.equals("PropertyReader")) {
			return READER.type;
		}
		
		return 0;
	}
}
