package com.ly.subway.subwaytools.dsfserver.metrics;

import com.codahale.metrics.MetricRegistry;
import com.ly.spat.dsf.entity.MetricsKey;

/**
 * metrics 组件
 * @author lwy32937
 *
 * @param <T>
 */
@FunctionalInterface
public interface RequestMetrics<T> {

	T apply(MetricsKey s);

	final static MetricRegistry metrics = new MetricRegistry();
}
