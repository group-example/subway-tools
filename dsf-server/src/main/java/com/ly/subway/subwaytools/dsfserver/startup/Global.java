package com.ly.subway.subwaytools.dsfserver.startup;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.Objects;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import com.ly.spat.dsf.utils.FileUtils;

/**
 * 全局类
 * 
 * @author lwy32937
 * @2016年7月6日 下午3:14:18
 */
public class Global {

//	private static final Logger logger = LoggerFactory.getLogger(Global.class);

	/**
	 * 每个服务一个类加载器
	 * 
	 * @param urls
	 * @return
	 */
	public DSFClassLoader getServiceLoader(URL[] urls) {
		return new DSFClassLoader(urls);
	}

	public void addSystemFolder(String path) throws Exception {
		File file = new File(path);
		// loadSystemJar(file); // 加载系统类
		loadSystemJar(file);
		loadAppClasses(file, Global.class.getClassLoader(), path);
	}

	public void loadSystemJar(File file) throws Exception {
		URLClassLoader loader = (URLClassLoader) ClassLoader.getSystemClassLoader();
		Method m = URLClassLoader.class.getDeclaredMethod("addURL", URL.class);
		Thread.currentThread().setContextClassLoader(loader);
		m.setAccessible(true);
		//加入classpath
		m.invoke(loader, file.toURI().toURL());
		if (file.isDirectory()) {
			File[] files = file.listFiles();
			for (File f : files) {
				if (f.getName().endsWith(".jar")) {
					m.invoke(loader, f.toURI().toURL()); // 加载jar
				}
			}
		}
	}

	/**
	 * 加载jar与处理注解
	 * 
	 * @param file
	 * @throws ClassNotFoundException
	 */
	public void readJar(File file) throws ClassNotFoundException {
		JarFile jarFile;
		try {
			jarFile = new JarFile(file);
			Enumeration<JarEntry> entrys = jarFile.entries();
			while (entrys.hasMoreElements()) {
				JarEntry entry = entrys.nextElement();
				if (entry.getName().endsWith(".class")) {
					String name = getFullName(entry.getName());
					Class.forName(name);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String getFullName(String value) {
		return value = (value.replace("/", ".").substring(0, value.length() - 6));
	}

	/**
	 * 加载应用的class es
	 * 
	 * @param loader
	 */
	public void loadAppClasses(File file, ClassLoader loader, String serviceDir) {
		File[] files = file.listFiles();
		if(Objects.isNull(files)) return;
		for (File f : files) {
			if (f.isDirectory()) {
				loadAppClasses(f, loader, serviceDir);
			}

			if (f.isFile() && f.getName().endsWith(".class")) {
				String path = FileUtils.getInstance().getUnixLikePath(serviceDir);
				String filePath = FileUtils.getInstance().getUnixLikePath(f.getAbsolutePath());
				String className = filePath.substring(path.length() + 1, filePath.length() - 6).replace("/", ".");
				try {
					Class.forName(className);
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public void loadAppJar(File file, DSFClassLoader loader) {
		File[] files = file.listFiles();
		if(Objects.isNull(files)) return;
		for (File f : files) {
			if (f.isFile() && f.getName().endsWith(".jar")) {
				try {
					loader.addFolder(f.getAbsolutePath()); // 加载jar
//					logger.debug("{} jar包加载成功.", f.getAbsolutePath());
				} catch (MalformedURLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public String getClsName(String name) {
		return name = name.substring(0, name.length() - 6);
	}

}
