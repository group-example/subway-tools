package com.ly.subway.subwaytools.dsfserver.utils;

import com.ly.spat.dsf.client.utils.ServerUtils;
import com.ly.spat.dsf.constants.Constants;
import com.ly.spat.dsf.entity.DSFResponse;
import com.ly.spat.dsf.factory.DSFThreadFactory;
import com.ly.spat.dsf.kernel.RegistryKernel;
import com.ly.spat.dsf.kernel.ServiceConfig;
import com.ly.spat.dsf.kernel.utils.KernelUtils;
import com.ly.spat.dsf.utils.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.*;

public class ServiceUtils {

	private final static Logger log = LoggerFactory.getLogger(ServiceUtils.class);

	private final ScheduledExecutorService sch = Executors.newSingleThreadScheduledExecutor(new DSFThreadFactory("dsf-serviceutils")); // 用于检查服务的注册状态

	private ServiceConfig config;
	
	private final static BlockingQueue<Runnable> loadQueue = new LinkedBlockingQueue<>(1);
	
	private ScheduledExecutorService pool = Executors.newSingleThreadScheduledExecutor(new DSFThreadFactory());
	
	private final CountDownLatch latch = new CountDownLatch(1);
	
	public ScheduledExecutorService getPool() {
		return pool;
	}

	public void setPool(ScheduledExecutorService pool) {
		this.pool = pool;
	}

	public ScheduledExecutorService getSch() {
		return sch;
	}

//	private ServiceUtils() {
//	}
//
//	public static ServiceUtils getInstance() {
//		if (tool == null) {
//			tool = new ServiceUtils();
//		}
//
//		return tool;
//	}

	/**
	 * kernel 拉起服务
	 * 
	 * @param kernel
	 */
	public void lookupService(final RegistryKernel kernel) {
		Thread t = new Thread(() -> {
			String path = System.getProperty(Constants.DSFHOME_ENV);
			if (StringUtils.isBlank(path)) {
				return;
			}

			//如果是springboot的项目，则执行启动
			if(isSpringbootProject(kernel,path))
				return;

			String servicePath = path + "/deploy";

			File file = new File(servicePath);
			if (!file.exists()){
				return ;
			}
			
			File[] files = file.listFiles();
			List<File> list = Arrays.asList(files);
			long total = list.stream().filter(f->{return !f.getName().equalsIgnoreCase("dsf.kernel");}).count();
			final ExecutorService service = Executors.newCachedThreadPool(new DSFThreadFactory());
			if (Objects.isNull(files) || total == 0) {
				String zipPath = path + "/dsfapps";
				File zipFile = new File(zipPath);
				if(!zipFile.exists()) {
					return;
				}
				
				List<File> zipfiles = Arrays.asList(zipFile.listFiles());
				
				zipfiles.stream().filter(f->{return !(f.getName().endsWith(".zip") && f.getName().equalsIgnoreCase("dsf.kernel.zip"));}).forEach(f ->{
					// decompress zip
					String name = FileUtils.getName(f.getName());
					String serviceDir = path + "/deploy/" + name;
					try {
						ZipUtils.decompress(serviceDir, f);
						service.submit(() ->{kernel.execute((byte) 1, name);});
					} catch (FileNotFoundException e) {
						log.error("拉起DSF服务失败", e);
					}
				});
				
			}else {
				list.stream().filter(f->{return !f.getName().equalsIgnoreCase("dsf.kernel");}).forEach(f->{
					log.debug("kernel pull up service {} start", f.getName());
					service.submit(() ->{kernel.execute((byte) 1, f.getName());});
					log.debug("kernel pull up service {} end", f.getName());
				});
			}
			
		}, "dsf-pull-service");

		t.setDaemon(true);
		t.start();
	}

	/**
	 *   判断是否是springboot项目，如果是，则拉起springboot的jar包，
	 * @param kernel
	 * @param path
	 * @return
	 */
	public boolean isSpringbootProject(final RegistryKernel kernel,String path)
	{
		try {
			String servicePath = path + "/dsfapps";
			File file = new File(servicePath);
			if (!file.exists())
			{
				log.error("dsfapps file not exist");
			}

			//scan the dsfapps file,cal the count of .jar
			File[] files = file.listFiles();
			List<File> list = Arrays.asList(files);
			long total = list.stream().filter(f->{return f.getName().endsWith(".jar");}).count();
			if(total==0)
			{
				log.info("this is not springboot project ");
				return false;
			}else
			{
				final ExecutorService service = Executors.newCachedThreadPool(new DSFThreadFactory());
				list.stream().filter(f->{return f.getName().endsWith(".jar");}).forEach(f->{
					String file_name = f.getName();
					String fileNameNojar=file_name.substring(0, file_name.lastIndexOf("."));
					log.debug("kernel pull up springbootservice {} start", fileNameNojar);

					service.submit(() ->{kernel.execute((byte) 5, fileNameNojar);});
					log.debug("kernel pull up springbootservice {} end", fileNameNojar);
				});
				return true;

			}

		}   catch (Exception e)
		{
			log.error("isSpringbootProject error",e);
		}
		return false;
	}


	/**
	 * 每5分钟检查一下服务是否注册，若没有，则注册（挂载)
	 */
	public void checkRegistry() {
		sch.scheduleAtFixedRate(() -> {
			register(config);
		}, 60 * 1000, 60 * 1000, TimeUnit.MILLISECONDS); // 1分钟检查一次
	}

	/**
	 * 启动完成后注册
	 * 
	 * @param config
	 */
	public void register(ServiceConfig config) {
		// 启动完成以后 注册
		try {
			this.config = config;
			/** 只能配置了kernelHost与kernelPort才进行挂载的动作 */
			if (!ValidTool.checkKernelConfig(config)) {
				return;
			}
			if (!StatusUtils.isRegistered().getAndSet(true)) {
				//上传全契约
				uploadFullContract(config);
				log.info("===========reflect holders :" + ReflectUtils.getServiceHolders());
				DSFResponse response = config.register(ReflectUtils.getServiceHolders());
				if (Objects.isNull(response))
					return;
				if (response.getStatus() == 200) {
					//上传全契约
					latch.countDown();
					// 启动心跳
					log.info("挂载服务组成功");
					new UploadStates(config).uploadStates();
					log.info("向kernel发送心跳成功");
				} else {
					throw new IllegalStateException("loading service:'" + config.getGsName() + "', version: '"  + config.getVersion() + "' failed");
				}

			}
		} catch (Exception e) {
			StatusUtils.isRegistered().set(false);
			log.error("register to center failed", e);
		}
	}
	
	public void uploadFullContract(ServiceConfig config){
		
		if(!StatusUtils.getIsUploadFullContract().getAndSet(true)) {
			Runnable r = () ->{
				try {
					latch.await(60, TimeUnit.SECONDS);
					DSFResponse response = config.uploadFullService(ReflectUtils.getServiceHolders());
					if(response.getStatus() == 200){
						log.info("上传全部契约成功");
					}
				} catch (Exception e) {
					e.printStackTrace();
					log.error("上传全部契约失败", e);
				}
			};
			
			Thread t = new Thread(r, "upload-full-contract");;
			t.start();
		}
	}
	
	/**
	 * 用单线程执行挂载kernel
	 */
	public void execReloading(){
		Thread t = new Thread(() -> {
			Runnable task;
			try {
				task = loadQueue.poll(10 * 1000, TimeUnit.MILLISECONDS);
				if(!Objects.isNull(task)) {
					task.run();
					log.info("reload kernel finished.");
				}else {
					log.debug("no reloading kernel task");
				}
			} catch (InterruptedException e) {
				log.debug("reloading kernel task failed " + e.getMessage());
			}
		});
		t.setDaemon(true);
		pool.scheduleAtFixedRate(t, 5 *1000, 5 * 1000, TimeUnit.MILLISECONDS);
	}
	
	/**
	 * reload
	 * @param kernel
	 */
	public void reloadTask(final RegistryKernel kernel, ServiceConfig config){
		try {
			loadQueue.add(new Runnable() {
				@Override
				public void run() {
					if (StatusUtils.getKernelRegisterd().get()) {
						try {
							KernelUtils.reloadGroup(kernel, NetUtils.pickIp(config.getIps()));
						} catch (Exception e) {
							log.error("reloading error", e);
						}
						return;
					}
					// getComponentPath kernel的时候去解析ip
					log.debug("kernel get center ip start.");
					String[] ips = config.getIps();
					if(Objects.isNull(ips) || ips.length == 0) {
						try {
							ips = ServerUtils.getHosts(config.getHost());
							config.setIps(ips);
						} catch (Exception e) {
							log.error("getComponentPath kernel error", e);
						}
					}
					log.debug("kernel get center ip success.");
					
					//尝试重新挂载
					List<String> ipslist = Arrays.asList(ips);
					Map<String, String> tempIpMap = new HashMap<>();
					tempIpMap.putAll(NetUtils.pickIp(config.getIps()));
					for (int i = 0; i < ipslist.size(); i++) {
						 boolean flag = kernel.loadKernel(ZmqReqType.KERNEL_LOAD, tempIpMap, config.getPort());
						if (flag) {
							break;
						}
						String newIp = ipslist.get(i);
						tempIpMap.put(Constants.CENTER_IP_KEY, newIp);
						NetUtils.pickIp(config.getIps()).clear();
					}
					
//					kernel.loadKernel(ZmqReqType.KERNEL_LOAD, NetUtils.pickIp(config.getIps()), config.getPort()); // 重新挂载
				}
			});
		} catch (Exception e) {
			log.error("reloading error", e);
		}
	}
}
