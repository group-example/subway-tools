package com.ly.subway.subwaytools.dsfserver.threads;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 线程池队列，重新实现take方法，使用线程idle，关闭
 * @author lwy32937
 *
 */
@SuppressWarnings("hiding")
public class TaskQueue<Runnable> extends LinkedBlockingQueue<Runnable> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	ThreadPoolExecutor parent = null;
		
	public TaskQueue(int initialCapacity) {
		super(initialCapacity);
	}
	
//	public void setParent(ThreadPoolExecutor tp){
//		parent = tp;
//	}

//	@Override
//	public Runnable take() throws InterruptedException {
//		
//		if(!Objects.isNull(parent)) {
//			return super.poll(parent.getKeepAliveTime(TimeUnit.SECONDS), TimeUnit.SECONDS);
//		}
//		
//		return super.take();
//	}
	
}
