package com.ly.subway.subwaytools.dsfserver.context;

import java.util.List;
import java.util.Map;

/**
 * http context
 * @author lwy32937
 * @2016年6月28日 下午4:42:11
 */
public class HttpContext {

	private String uri; //
	
	private byte[] body; 
	
	private Map<String,List<String>> params; 
	
	private Map<String, List<String>> headers;

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public byte[] getBody() {
		return body;
	}

	public void setBody(byte[] body) {
		this.body = body;
	}

	public Map<String, List<String>> getParams() {
		return params;
	}

	public void setParams(Map<String, List<String>> params) {
		this.params = params;
	}

	public Map<String, List<String>> getHeaders() {
		return headers;
	}

	public void setHeaders(Map<String, List<String>> headers) {
		this.headers = headers;
	} 
	
	
}
