package com.ly.subway.subwaytools.dsfserver.function;

import com.ly.spat.dsf.utils.StringUtils;
import io.netty.buffer.Unpooled;
import io.netty.handler.codec.http.*;

import java.io.UnsupportedEncodingException;

@FunctionalInterface
public interface ResponseBuilder<T> {

	T apply(String res);

	static DefaultFullHttpResponse response(String res) {
		DefaultFullHttpResponse response = null;
		if (StringUtils.isNotBlank(res)) {
			try {
				response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK,
						Unpooled.wrappedBuffer(res.getBytes("UTF-8")));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		} else {
			response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK);
		}
		response.headers().set(HttpHeaderNames.CONTENT_TYPE, "application/json");
		response.headers().set(HttpHeaderNames.CONTENT_LENGTH, response.content().readableBytes());
		response.headers().set(HttpHeaderNames.CONNECTION, HttpHeaderValues.KEEP_ALIVE);
		return response;
	};
	
}
