package com.ly.subway.subwaytools.dsfserver.monitor;

import java.util.Objects;
import java.util.function.Supplier;

/**
 * @author lwy32937
 * @create 2017/11/9
 **/
public final class ObjectUtil {

    /**
     * 不为null切不为空
     * @param res
     * @return
     */
    public static boolean nonNullOrEmpty (String res) {
        return Objects.nonNull(res) && !"".equals(res);
    }

    /**
     * 为null或者empty
     * @param res
     * @return
     */
    public static boolean nullOrEmpty (Object res) {
        return res == null || "".equals(String.valueOf(res));
    }

    /**
     * 返回非空的该对象
     * @param res
     * @param message
     * @return
     */
    public static <T> T requireNonNull(T res, String message) {
        return Objects.requireNonNull(res, message);
    }

    /**
     * 返回非空的该对象
     * @param res
     * @param message
     * @return
     */
    public static <T> T requireNonNullOrEmpty(T res, String message) {
        if (nullOrEmpty(res)) {
            throw new NullPointerException(message);
        }

        return res;
    }

    /**
     * not null check
     * @param res
     * @return
     */
    public static boolean nonNull(Object res) {
        return Objects.nonNull(res);
    }

    /**
     * 不为null切不为0
     * @param res
     * @return
     */
    public static boolean nonNullOrZero(Integer res){
        return ObjectUtil.nonNull(res) && res.intValue() != 0;
    }

    /**
     * is null
     * @param res
     * @return
     */
    public static boolean isNull(Object res) {
        return Objects.isNull(res);
    }

    /**
     * 校验参数
     * @param expression 布尔判断表达式
     * @param message
     * @return
     */
    public static void checkArgument(boolean expression, String message) {
        if (!expression) {
            throw new IllegalArgumentException(message);
        }
    }

    /**
     * 检查状态
     * @param expression
     * @param message
     */
    public static void checkState(boolean expression, String message) {
        if (!expression) {
            throw new IllegalStateException(message);
        }
    }

    /**
     * check state
     * @param s
     * @param message
     */
    public static void checkState(Supplier<Boolean> s, String message) {
        if (!s.get()) {
            throw new IllegalStateException(message);
        }
    }


}
