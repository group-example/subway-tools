package com.ly.subway.subwaytools.dsfserver.monitor;

/**
 * 运行时状态bean
 * @author lwy32937
 * 2016年8月16日 下午4:05:33
 */
public class MonitorBean {

	private long freeMemory; 
	
	private double cpuUsed; 
	
	private long totalMemory;
	
	private double memeryUsed; //利用
	
	private int threads; //线程
	
	private int waitCount; //等待队列

	public long getFreeMemory() {
		return freeMemory;
	}

	public void setFreeMemory(long freeMemory) {
		this.freeMemory = freeMemory;
	}

	public double getCpuUsed() {
		return cpuUsed;
	}

	public void setCpuUsed(double cpuUsed) {
		this.cpuUsed = cpuUsed;
	}

	public long getTotalMemory() {
		return totalMemory;
	}

	public void setTotalMemory(long totalMemory) {
		this.totalMemory = totalMemory;
	}

	public double getMemeryUsed() {
		return memeryUsed;
	}

	public void setMemeryUsed(double memeryUsed) {
		this.memeryUsed = memeryUsed;
	}

	public int getThreads() {
		return threads;
	}

	public void setThreads(int threads) {
		this.threads = threads;
	}

	public int getWaitCount() {
		return waitCount;
	}

	public void setWaitCount(int waitCount) {
		this.waitCount = waitCount;
	}
	
}
