package com.ly.subway.subwaytools.dsfserver.monitor;

import com.ly.spat.dsf.factory.DSFThreadFactory;
import com.sun.management.OperatingSystemMXBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.management.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 对服务的监控 上传到注册中心
 *
 * @author lwy32937 2016年8月16日 下午3:28:43
 */
public class DsfMonitor {

    public static final ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor(new DSFThreadFactory("dsfstarter-monitor"));

    private MonitorBean bean = new MonitorBean();

    //操作系统
    private static final OperatingSystemMXBean mxBean = ManagementFactory.getPlatformMXBean(OperatingSystemMXBean.class);

    private static final MemoryMXBean memoryBean = ManagementFactory.getMemoryMXBean();

    private static final RuntimeMXBean runtimeBean = ManagementFactory.getRuntimeMXBean();

    private static final ThreadMXBean threadBean = ManagementFactory.getThreadMXBean();

    private long previousJvmProcessCpuTime;

    private long previousJvmUptime;

    private JVMCPUUsage us;

    private static DockerMonitor monitor = new DockerMonitor.Builder().build();

    private static final Logger log = LoggerFactory.getLogger(DsfMonitor.class);

    private static volatile MonitorBean monitorBean;

    public static MonitorBean getBean() {
        return monitorBean;
    }

    /**
     * java get metrics
     *
     * @param executor
     * @return
     */
    public MonitorBean getMonitorBean(ThreadPoolExecutor executor) {

        String osName = System.getProperty("os.name");
        if (osName.contains("Windows")) {
            long currentJvmProcessTime = mxBean.getProcessCpuTime();

            log.info("currentJvmProcessTime :{}, previousJvmProcessCpuTime :{}", currentJvmProcessTime, previousJvmProcessCpuTime);
            long elapsedJvmCpuTime = currentJvmProcessTime - previousJvmProcessCpuTime;
            long elapsedJvmUpTime = runtimeBean.getUptime() - previousJvmUptime;
            long totalElapsedJvmUptime = elapsedJvmUpTime * mxBean.getAvailableProcessors();
            if (previousJvmUptime == 0) {
                bean.setCpuUsed(0);
            } else {
                bean.setCpuUsed(Double.valueOf(String.format("%.2f", elapsedJvmCpuTime / (totalElapsedJvmUptime * 10000F))));
            }
            previousJvmProcessCpuTime = mxBean.getProcessCpuTime();
            previousJvmUptime = runtimeBean.getUptime();

            bean.setThreads(threadBean.getThreadCount());
            if (ObjectUtil.nonNull(executor)) {
                bean.setWaitCount(executor.getQueue().size());
            }
            MemoryUsage heap = memoryBean.getHeapMemoryUsage();
            MemoryUsage nonHeap = memoryBean.getNonHeapMemoryUsage();
            bean.setTotalMemory((heap.getMax() + nonHeap.getMax()) / 1024); //KB
            bean.setMemeryUsed((heap.getUsed() + nonHeap.getUsed()) / 1024);//KB
            bean.setFreeMemory((heap.getMax() + nonHeap.getMax() - heap.getUsed() - nonHeap.getUsed()) / 1024);//MB

            monitorBean = bean;
        } else if (osName.contains("Mac")) {
			       bean.setCpuUsed(2.5);
			bean.setFreeMemory(3l);
			bean.setMemeryUsed(1l);
			monitorBean=bean;

		} else {
			DockerStates states = monitor.getMetrics();
			DockerStates.Memusage memusage = states.getMemusage();
			DockerStates.Cpusage cpusage = states.getCpusage();

			if (ObjectUtil.nonNull(memusage) && ObjectUtil.nonNull(cpusage)) {
				log.info("cpu usage :{}, mem total:{}, mem used:{}", cpusage.getCpusage(), memusage.getLimit(), memusage.getRss());
				bean.setCpuUsed(cpusage.getCpusage());
				bean.setFreeMemory((long) memusage.getFree());
				bean.setTotalMemory((long) memusage.getLimit());
				bean.setMemeryUsed(memusage.getRss());
				bean.setThreads(threadBean.getThreadCount());
				bean.setWaitCount(executor.getQueue().size()); //等待队列
				monitorBean = bean;
			}
		}

        return monitorBean;
    }
}
