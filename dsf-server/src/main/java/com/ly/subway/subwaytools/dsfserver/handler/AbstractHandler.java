package com.ly.subway.subwaytools.dsfserver.handler;

import java.io.UnsupportedEncodingException;
import java.util.Objects;

import com.ly.spat.dsf.context.DSFContext;
import com.ly.spat.dsf.exception.DSFException;
import com.ly.spat.dsf.handler.Handler;
import com.ly.spat.dsf.serializer.JsonSerializer;
import com.ly.spat.dsf.utils.StringUtils;
import io.netty.buffer.Unpooled;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpHeaderValues;
import io.netty.handler.codec.http.HttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;

/**
 * 抽象handler
 * 
 * @author lwy32937
 * @2016年7月1日 下午5:25:08
 */
public abstract class AbstractHandler implements Handler {

	private final JsonSerializer serializer = new JsonSerializer();

	abstract public void handlePost(DSFContext ctx) throws Exception;

	abstract public void handleGet(DSFContext ctx) throws Exception;
	
	protected HttpResponse buildResponse(Object obj, DSFContext ctx) {
		String res = null; 
		if(!Objects.isNull(obj)) {
			res = serializer.toJson(obj);
		}
		FullHttpResponse response = null;
		try {
			response = (StringUtils.isBlank(res) ? new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK) : new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK,
					Unpooled.wrappedBuffer(res.getBytes("UTF-8"))));
			response.headers().set(HttpHeaderNames.CONTENT_TYPE, "application/json");
			response.headers().set(HttpHeaderNames.CONTENT_LENGTH, response.content().readableBytes());
			response.headers().set(HttpHeaderNames.CONNECTION, HttpHeaderValues.KEEP_ALIVE);
		} catch (UnsupportedEncodingException e) {
			throw new DSFException("build response error.", e);
		}

		return response;
	}
	
	protected HttpResponse buildErrResponse(Object obj, HttpResponseStatus status,  DSFContext ctx) {
		String res = serializer.toJson(obj);
		FullHttpResponse response = null;
		try {
			response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, status,
					Unpooled.wrappedBuffer(res.getBytes("UTF-8")));
			response.headers().set(HttpHeaderNames.CONTENT_TYPE, "application/json");
			response.headers().set(HttpHeaderNames.CONTENT_LENGTH, response.content().readableBytes());
			response.headers().set(HttpHeaderNames.CONNECTION, HttpHeaderValues.KEEP_ALIVE);
		} catch (UnsupportedEncodingException e) {
			throw new DSFException("build response error.", e);
		}

		return response;
	}

	
}
