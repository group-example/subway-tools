package com.ly.subway.subwaytools.dsfserver.protocol;

import io.netty.handler.codec.http.DefaultFullHttpRequest;
import io.netty.handler.codec.http.DefaultFullHttpResponse;

/**
 * http
 * @author lwy32937
 * 2016年9月5日 上午10:09:22
 */
public class HttpProtocol implements Protocol {

	//netty request
	private DefaultFullHttpRequest request; 
	
	private DefaultFullHttpResponse response;

	public DefaultFullHttpRequest getRequest() {
		return request;
	}

	public void setRequest(DefaultFullHttpRequest request) {
		this.request = request;
	}

	public DefaultFullHttpResponse getResponse() {
		return response;
	}

	public void setResponse(DefaultFullHttpResponse response) {
		this.response = response;
	} 
	
}
