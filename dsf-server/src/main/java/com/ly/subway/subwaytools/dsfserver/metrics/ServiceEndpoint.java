package com.ly.subway.subwaytools.dsfserver.metrics;

import java.util.List;

/**
 * 服务节点
 * @author lwy32937
 * 2016年9月6日 下午2:50:34
 */
public class ServiceEndpoint {

	
	private String Endpoint; 
	
	private int ProcessingRequests; 
	
	private List<State> States; 
	
	
	public static class State {
		
		private String SName; 
		
		private String ActionName; 
		
		private List<Consumers> Consumers;

		public String getSName() {
			return SName;
		}

		public void setSName(String sName) {
			SName = sName;
		}

		public String getActionName() {
			return ActionName;
		}

		public void setActionName(String actionName) {
			ActionName = actionName;
		}

		public List<Consumers> getConsumers() {
			return Consumers;
		}

		public void setConsumers(List<Consumers> consumers) {
			Consumers = consumers;
		} 
		
	}
	
	/**
	 * Consumers
	 * @author lwy32937
	 * 2016年9月6日 下午2:53:30
	 */
	public static class Consumers {
		private String IP; 
		
		private String Name; 
		
		private long Count; 
		
		private long Fail; 
		
		private long Max; 
		
		private long Min;
		
		private double Avg; 
		
		private double Percentile99; 
		
		private double Percentile98;
		
		private double Percentile95;
		
		private double Percentile75;

		public String getIP() {
			return IP;
		}

		public void setIP(String iP) {
			IP = iP;
		}

		public String getName() {
			return Name;
		}

		public void setName(String name) {
			Name = name;
		}

		public long getCount() {
			return Count;
		}

		public void setCount(long count) {
			Count = count;
		}

		public long getFail() {
			return Fail;
		}

		public void setFail(long fail) {
			Fail = fail;
		}

		public long getMax() {
			return Max;
		}

		public void setMax(long max) {
			Max = max;
		}

		public long getMin() {
			return Min;
		}

		public void setMin(long min) {
			Min = min;
		}

		public double getAvg() {
			return Avg;
		}

		public void setAvg(double avg) {
			Avg = avg;
		}

		public void setAvg(int avg) {
			Avg = avg;
		}

		public double getPercentile99() {
			return Percentile99;
		}

		public void setPercentile99(double percentile99) {
			Percentile99 = percentile99;
		}

		public double getPercentile98() {
			return Percentile98;
		}

		public void setPercentile98(double percentile98) {
			Percentile98 = percentile98;
		}

		public double getPercentile95() {
			return Percentile95;
		}

		public void setPercentile95(double percentile95) {
			Percentile95 = percentile95;
		}

		public double getPercentile75() {
			return Percentile75;
		}

		public void setPercentile75(double percentile75) {
			Percentile75 = percentile75;
		}
		
	}

	public String getEndpoint() {
		return Endpoint;
	}

	public void setEndpoint(String endpoint) {
		Endpoint = endpoint;
	}

	public int getProcessingRequests() {
		return ProcessingRequests;
	}

	public void setProcessingRequests(int processingRequests) {
		ProcessingRequests = processingRequests;
	}

	public List<State> getStates() {
		return States;
	}

	public void setStates(List<State> states) {
		States = states;
	}
	
	
}
