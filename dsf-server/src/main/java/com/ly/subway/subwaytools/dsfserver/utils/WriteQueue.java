package com.ly.subway.subwaytools.dsfserver.utils;

import java.nio.channels.Channel;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by IntelliJ IDEA.
 * User: lwy32937
 * Date: 2017/9/30
 * Time: 18:09
 * Description:
 */
public class WriteQueue {

    private BlockingQueue<Channel> queue = new LinkedBlockingQueue<>();


    public void addChannel(Channel ch){
        if (!queue.contains(ch)) {
            queue.add(ch);
        }
    }


}