package com.ly.subway.subwaytools.dsfserver.handler;

import com.ly.spat.dsf.context.DSFContext;
import com.ly.spat.dsf.context.DSFInvocation;
import com.ly.spat.dsf.filter.FilterChain;
import com.ly.spat.dsf.handler.Handler;
import com.ly.spat.dsf.kernel.ServiceConfig;
import com.ly.spat.dsf.serializer.JsonSerializer;
import com.ly.spat.dsf.serializer.Serializer;
import com.ly.spat.dsf.utils.StatusUtils;
import com.ly.subway.subwaytools.dsfserver.function.ResponseBuilder;
import com.ly.subway.subwaytools.dsfserver.startup.DSFApplicationEnv;
import com.ly.subway.subwaytools.dsfserver.utils.ServiceUtils;
import com.ly.subway.subwaytools.dsfserver.utils.WriteUtils;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.CompositeByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.*;
import io.netty.util.CharsetUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import java.net.InetSocketAddress;

/**
 * http handler
 *
 * @author lwy32937
 * @2016年6月23日 下午4:39:39
 */
public class HttpHandler extends ChannelInboundHandlerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(HttpHandler.class);

    private final FilterChain filterChain;

    private static final Serializer serializer = new JsonSerializer();

    private HttpRequest req;

    private DSFContext context;

    private final StringBuilder sb = new StringBuilder();

    private final CompositeByteBuf compositeByteBuf = Unpooled.compositeBuffer(32);

    public HttpHandler(FilterChain filterChain) {
        this.filterChain = filterChain;
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        ctx.flush();
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        //logger.debug("channel :{}, handler:{}, message:{}", ctx.channel(), this, msg);
        if (msg instanceof HttpRequest) {
            try {
                //清空
                sb.setLength(0);
                //logger.debug("http request start");
                req = (HttpRequest) msg;
                //初始化请求f
                context = initReq(ctx, msg, req);

                if (!req.decoderResult().isSuccess()) {
                    WriteUtils.write("请求无效， 解码失败", HttpResponseStatus.BAD_REQUEST, context);
                    return;
                }
                // 过滤器
                if (filterChain != null) {
                    FullHttpResponse fhp = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK);
                    try {
                        if (!filterChain.doFiter(req, fhp)) {
                            WriteUtils.write("请求被过滤，请检查权限", HttpResponseStatus.INTERNAL_SERVER_ERROR, context);
                            return;
                        }
                    } catch (Exception e) {
                        logger.error("filter error", e);
                        if (e instanceof com.ly.spat.dsf.filter.FilterException) {
                            WriteUtils.write(e.getMessage(), HttpResponseStatus.TEMPORARY_REDIRECT, context);
                            return;
                        }
                        WriteUtils.write(e.getMessage(), HttpResponseStatus.INTERNAL_SERVER_ERROR, context);
                        return;
                    } finally {
                        ;
                    }

                }
            } catch (Exception e) {
                logger.error("server internal error", e);
                WriteUtils.write(e.getMessage(), HttpResponseStatus.INTERNAL_SERVER_ERROR, context);
                return;
            }
        }

        if (msg instanceof HttpContent) {
            HttpContent content = (HttpContent) msg;
            ByteBuf bytebuf = content.content();
            compositeByteBuf.addComponent(true, bytebuf);
            if (msg instanceof LastHttpContent) {
                //校验请求
                if (!validRequest(ctx, context)) return;
                context.getInvocation().setRequestBody(compositeByteBuf.toString(CharsetUtil.UTF_8));
                compositeByteBuf.clear();
                compositeByteBuf.removeComponents(0, compositeByteBuf.numComponents());
                buildChainHandler().handle(context);
            }
        }
    }

    private DSFContext initReq(ChannelHandlerContext ctx, Object msg, HttpRequest req) {
        DSFContext context = initContext(ctx, req);
        boolean isKeeplive = HttpUtil.isKeepAlive(req);
        context.setKeeplive(isKeeplive);
        context.setcName(req.headers().get("CName"));
        return context;
    }

    private boolean validRequest(ChannelHandlerContext ctx, DSFContext context) {
        HttpRequest req = context.getRequest();

        if (!StatusUtils.getIsserving().get()) {
            WriteUtils.write("server is shutdown", HttpResponseStatus.TEMPORARY_REDIRECT, context);
            return false;
        }

        String uri = req.uri();

        // "/dsfstarter/, /dsfstarter, /"三个都是健康检查的接口
        if (uri.equalsIgnoreCase("/dsfstarter/") || uri.equalsIgnoreCase("/dsfstarter") || uri.equalsIgnoreCase("/")) {
            ctx.writeAndFlush(ResponseBuilder.response(serializer.toJson("server is ok")));
            return false;
        }
        if (req.uri().equalsIgnoreCase("/favicon.ico"))
            return false;

        //服务要执行关闭
        if (req.uri().equalsIgnoreCase("/dsfstarter/stop")) {
            logger.info("服务正在执行关闭");
            StatusUtils.getIsserving().set(false);//服务禁止访问 //不发心跳
            StatusUtils.isRegistered().set(false);
            WriteUtils.write("server is shutting down", HttpResponseStatus.OK, context);
            return false;
        }

        //重新挂载服务
        if (req.uri().equalsIgnoreCase("DSFserviceReload")) {
            ServiceUtils utils = new ServiceUtils();
            ApplicationContext applicationContext= DSFApplicationEnv.getContext();
            utils.register(applicationContext.getBean(ServiceConfig.class));
        }

        return true;
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }


    /**
     * 初始化 dsf上下文
     *
     * @param ctx
     * @param req
     * @return
     */
    private DSFContext initContext(ChannelHandlerContext ctx, HttpRequest req) {
        DSFContext dsfContext = new DSFContext(); // init 上下文
        dsfContext.setRequest(req);
        dsfContext.setContext(ctx);
        dsfContext.setChannel(ctx.channel());
        dsfContext.setRequestIp(((InetSocketAddress) ctx.channel().remoteAddress()).getAddress().getHostAddress()); // 加入客户端地址
        dsfContext.setInvocation(new DSFInvocation());
        dsfContext.getWatch().start();

//		String messageId = req.headers().get("message_id");
//		dsfContext.setMessageId(messageId);
        return dsfContext;
    }

    /**
     * 用handler chain 来处理请求
     *
     * @return
     */
    private Handler buildChainHandler() {
        return Chains.build();
    }

}
