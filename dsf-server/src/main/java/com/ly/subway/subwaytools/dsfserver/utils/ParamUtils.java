package com.ly.subway.subwaytools.dsfserver.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.ly.spat.dsf.exception.DSFException;

/**
 * JSF 参数处理类
 * 
 * @author lwy32937
 *
 */
public class ParamUtils {
	
	private static Logger logger = Logger.getLogger(ParamUtils.class.toString());

	private static ParamUtils INSTANCE = new ParamUtils();

	private ParamUtils() {
	}

	/**
	 * 
	 * @return
	 */
	public static ParamUtils getInstance() {
		if (INSTANCE == null) {
			synchronized (ParamUtils.class) {
				if (INSTANCE == null)
					INSTANCE = new ParamUtils();
			}
		}

		return INSTANCE;
	}
	
	/**
	 * 处理参数是实体类
	 * @param info
	 * @param params
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 */
	public Object processWithCls(Class<?> type , Method [] methods, Map<String, List<String>> params)  throws Exception{
		Object instance ;
		try {
			instance = type.newInstance();
			for(Method method : methods) {
				String name = method.getName();
				if(name.startsWith("set")) {
					String key = name.substring(3, name.length()).toLowerCase();
					String param = (params.get(key) == null || params.get(key).size() == 0) ? "" : params.get(key).get(0); 
					method.invoke(instance, param);
				}
				continue;
			}
		} catch(Exception e) {
			logger.log(Level.WARNING, "params not match with the given.");
			throw new DSFException("params not match with the given.", e) ;
		}
		
		return instance; 
	}
	
//
//	public static void main(String[] args) {
//	}
}


