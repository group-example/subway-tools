package com.ly.subway.subwaytools.dsfserver;

import com.ly.spat.dsf.client.v2.DSFProxy;
import com.ly.spat.dsf.constants.Constants;
import com.ly.spat.dsf.entity.StatusType;
import com.ly.spat.dsf.kernel.RegistryKernel;
import com.ly.spat.dsf.kernel.ServiceConfig;
import com.ly.spat.dsf.utils.CVTUtils;
import com.ly.spat.dsf.utils.StringUtils;
import com.ly.subway.subwaytools.dsfserver.transport.HttpServer;
import com.ly.subway.subwaytools.dsfserver.utils.ValidTool;
import com.ly.tcbase.config.AppProfile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * start the DSF server
 *
 * @author lwy32937
 * @2016年6月29日 下午2:15:03
 */
@Configuration
@Import(value = {ServiceConfig.class})
public class DSF {

    private static final Logger logger = LoggerFactory.getLogger(DSF.class);

    @Autowired(required = true)
    private ServiceConfig config;

    @Autowired
    private RegistryKernel kernel;

    public ServiceConfig getConfig() {
        return config;
    }

    public void setConfig(ServiceConfig config) {
        this.config = config;
    }

    public RegistryKernel getKernel() {
        return kernel;
    }

    public void setKernel(RegistryKernel kernel) {
        this.kernel = kernel;
    }

    /**
     * 初始化 server
     *
     * @throws Exception
     */
    public void init(String name) {
        try {
            getEnv(name);
            ValidTool.checkConfig(config);
            final Server server = new HttpServer(config, kernel);
            try {
                server.start();
            } catch (Exception e) {
                logger.error("server init failed", e);
                if (server != null) {
                    server.stop();
                }
            }
        } catch (Exception e) {
            logger.error("服务启动失败", e);
            System.exit(0);
        }
    }

    /**
     * 读取ENV的配置
     *
     * @param name
     */
    public void getEnv(String name) throws Exception {
        config.setFolderName(name);
        config.setStatus(StatusType.RUNNING); // 服务状态转为运行中

        //取应用标识
        String gsName = System.getenv(Constants.DAOKEAPPUK);
        if (StringUtils.isNotBlank(gsName)) {
            config.setGsName(gsName);
        }

        //优先读环境变量
        String env = System.getenv(Constants.DOCKER_ENV);
        if (StringUtils.isNotBlank(env)) { // 根据环境重新设置env
            List<String> envList = Arrays.asList(Constants.ENV);
            if (!envList.contains(env.toLowerCase())) {
                config.setEnv(env);
                config.setGsName(env + "." + config.getGsName());
            }

        } else {
            //按配置文件
            env = config.getEnv();
            if (StringUtils.isNotBlank(env)) {
                env = env.toLowerCase();
                List<String> envList = Arrays.asList(Constants.ENV);
                if (!envList.contains(env.toLowerCase())) {
                    config.setEnv(env);
                    config.setGsName(env + "." + config.getGsName());
                }
            }
        }
        if (StringUtils.isEmpty(env)) {
            throw new Exception("未获取到运行环境env");
        } else {
            logger.info("当前环境env={}", env);
        }

        // registry host
        String registryhost = AppProfile.getEnvironment();
        if (StringUtils.isNotBlank(registryhost)) {
            config.setHost(registryhost);
        } else {
            RegistryHostEnum hostEnum = RegistryHostEnum.getByEnv(env);
            if (hostEnum == null) {
                throw new Exception("未查找到env对应的RegistryHost，请修正env参数");
            }
            config.setHost(hostEnum.getHost());
        }
        logger.info("当前注册中心host={}", config.getHost());

        // env port kernel使用PORT0, container:PORT1
        if (config.isKernel()) {
            Integer PORT0 = CVTUtils.getPORT("PORT0");
            if (!Objects.isNull(PORT0)) {
                config.setPort(PORT0);
            }
        } else {
            Integer PORT1 = CVTUtils.getPORT("PORT1");
            if (!Objects.isNull(PORT1)) {
                config.setPort(PORT1);
            }

            Integer PORT0 = CVTUtils.getPORT("PORT0");
            if (!Objects.isNull(PORT0)) {
                config.setKernelPort(PORT0);
            }

        }


        String kernel_host = System.getenv(Constants.KERNEL_HOST);
        if (StringUtils.isNotBlank(kernel_host)) {
            config.setKernelHost(kernel_host);
        }

    }

    @Bean(name = "defaultProxy")
    public DSFProxy getDSFProxy() {
        return DSFProxy.newInstance();
    }

    enum RegistryHostEnum {

        TEST("test", "qa.dsf2.17usoft.com"),
        QA("qa", "qa.dsf2.17usoft.com"),
        STAGE("stage", "t.dsf2.17usoft.com"),
        PRODUCT("product", "dsf2.17usoft.com");

        private String env;
        private String host;

        RegistryHostEnum(String env, String host) {
            this.env = env;
            this.host = host;
        }

        public static RegistryHostEnum getByEnv(String env) {
            for (RegistryHostEnum enu : RegistryHostEnum.values()) {
                if (enu.env.equals(env)) {
                    return enu;
                }
            }
            return null;
        }

        public String getEnv() {
            return env;
        }

        public String getHost() {
            return host;
        }
    }
}
