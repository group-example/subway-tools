package com.ly.subway.subwaytools.dsfserver.utils;

import com.google.gson.GsonBuilder;
import com.ly.spat.dsf.annoation.HttpType;
import com.ly.spat.dsf.context.DSFContext;
import com.ly.spat.dsf.exception.DSFException;
import com.ly.spat.dsf.serializer.JsonSerializer;
import com.ly.spat.dsf.serializer.Serializer;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFutureListener;
import io.netty.handler.codec.http.*;
import io.netty.util.AsciiString;
import io.netty.util.concurrent.EventExecutor;

import java.io.UnsupportedEncodingException;
import java.util.Objects;

/**
 * print response
 * 
 * @author lwy32937 2017年2月27日
 *
 */
public class WriteUtils {

	private static final AsciiString CONNECTION = new AsciiString("Connection");

	private final static GsonBuilder builder = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.SSS").disableHtmlEscaping();

	private final static Serializer serializer = new JsonSerializer(builder);

	/**
	 * 返回信息
	 * 
	 * @param ctx
	 * @param message
	 * @param status
	 */
	public static void write(Object message, HttpResponseStatus status, DSFContext ctx) {
		//默认都是json
		write(message, status, ctx, HttpType.APPLICATION_JSON);
	}

	/**
	 * 返回信息
	 *
	 * @param ctx
	 * @param message
	 * @param status
	 */
	public static void write(Object message, HttpResponseStatus status, DSFContext ctx, HttpType httpType) {
		Channel channel = ctx.getChannel();
		try {
			//交给IO线程组
			HttpResponse response = buildResponse(message, status, ctx, httpType);
			EventExecutor executor = ctx.getContext().executor();

			if (executor.inEventLoop()) {
				ctx.getContext().writeAndFlush(response);
			}else {
				executor.execute(() -> {
					if (!ctx.isKeeplive()) {
						if(channel.isWritable()) {
							channel.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
						}
					} else {
						response.headers().set(CONNECTION, HttpHeaderValues.KEEP_ALIVE);
						if(channel.isWritable()) {
							channel.writeAndFlush(response);
						}
					}
				});
			}
		} finally {
			;
		}
	}

	private static HttpResponse buildResponse(Object obj, HttpResponseStatus status, DSFContext ctx, HttpType httpType) {
		String res = null;
		FullHttpResponse response = ctx.getResponse();
		if (!Objects.isNull(obj)) {
			response.headers().set(HttpHeaderNames.CONTENT_TYPE, httpType.getValue());
			if (obj instanceof String) {
				res = (String)obj;
			}else {
				res = serializer.toJson(obj);
			}
		}

		try {
			if (Objects.isNull(res))
				response = response.setStatus(status);
			else {
				response.setStatus(status);
				response.content().writeBytes(Unpooled.wrappedBuffer(res.getBytes("UTF-8")));
			}

			response.headers().set(HttpHeaderNames.CONTENT_LENGTH, response.content().readableBytes());
		} catch (UnsupportedEncodingException e) {
			throw new DSFException("build response error.", e);
		}

		return response;
	}

}
