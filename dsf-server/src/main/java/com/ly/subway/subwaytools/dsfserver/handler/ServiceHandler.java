package com.ly.subway.subwaytools.dsfserver.handler;

import com.ly.spat.dsf.annoation.RequestMethod;
import com.ly.spat.dsf.constants.Constants;
import com.ly.spat.dsf.context.DSFContext;
import com.ly.spat.dsf.entity.ClassInfo;
import com.ly.spat.dsf.entity.URI;
import com.ly.spat.dsf.exception.ServiceNotExistsException;
import com.ly.spat.dsf.handler.Handler;
import com.ly.spat.dsf.utils.HandlerUtils;
import com.ly.spat.dsf.utils.ReflectUtils;
import com.ly.subway.subwaytools.dsfserver.startup.DSFApplicationEnv;
import com.ly.subway.subwaytools.dsfserver.utils.WriteUtils;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpResponseStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import java.util.Objects;

/**
 * 寻找提供真正提供服务的service -->Service Router
 *
 * @author lwy32937
 * @2016年6月30日 上午9:33:24
 */
public class ServiceHandler extends AbstractHandler implements Handler {

    private static final Logger logger = LoggerFactory.getLogger(ServiceHandler.class);

    private Handler handler;

    public ServiceHandler(Handler handler) {
        this.handler = handler;
    }

    @Override
    public void handle(DSFContext context) {
        HttpMethod httpMethod = context.getRequest().method();
        String mname = httpMethod.name();
        try {
            switch (mname) {
                case "GET":
                    handleGet(context);
                    break;
                case "POST":
                    handlePost(context);
                    break;
                case "HEAD":
                    WriteUtils.write(null , HttpResponseStatus.OK, context);
                    break;
                default:
                    WriteUtils.write(HttpResponseStatus.METHOD_NOT_ALLOWED.reasonPhrase(), HttpResponseStatus.METHOD_NOT_ALLOWED, context);
            }

        } catch (Exception e) {
            logger.error("请求失败", e);
            WriteUtils.write(Constants.SERVICE_INTERNAL_ERROR + e.getMessage(), HttpResponseStatus.BAD_REQUEST, context);
        }

    }

    private ClassInfo getClassInfo(String uri) throws ServiceNotExistsException {
        return ReflectUtils.getClassInfo(ReflectUtils.getServiceContract(uri));
    }

    @Override
    public void handlePost(DSFContext ctx) throws ServiceNotExistsException {
        getInvocation(ctx);
        handler.handle(ctx);
    }

    @Override
    public void handleGet(DSFContext ctx) throws ServiceNotExistsException {
        getInvocation(ctx);
        handler.handle(ctx);
    }

    /**
     * 取得ctx invocation 全局的请求参数 (针对一次请求）
     *
     * @param ctx
     * @throws ServiceNotExistsException
     * @throws Exception
     */
    private void getInvocation(DSFContext ctx) throws ServiceNotExistsException {
        URI uri = HandlerUtils.getInstance().getURI(ctx.getRequest().uri());
        if (uri == null)
            logger.error("request uri is empty, request ip is :{}", ctx.getRequestIp());
        String _uri = null;
        _uri = uri.getRequestURI();
        ctx.getInvocation().setUri(_uri);

        ClassInfo info = getClassInfo(_uri); // 通过uri获取真正接口
        if (Objects.isNull(info)) {
            throw new ServiceNotExistsException("'" + _uri + "' service could not found.");
        }

        // 如果不是default，则限制类型， 如果是default， 则get post 都可以调
        RequestMethod requestType = info.getMethodInfo().get(_uri).getType();
        if (!requestType.equals(RequestMethod.DEFAULT)) {
            if (!ctx.getType().equals(requestType)) {
                throw new IllegalStateException(" can not found service for " + _uri + " with '" + ctx.getType() + "'");
            }
        }
        ctx.getInvocation().setInfo(info);
        // 从spring容器中取bean
        ApplicationContext applicationContext= DSFApplicationEnv.getContext();
        ctx.getInvocation().setObj(applicationContext.getBean(ctx.getInvocation().getInfo().getCls()));
    }

}
