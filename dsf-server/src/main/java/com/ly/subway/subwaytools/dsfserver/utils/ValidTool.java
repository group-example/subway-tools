package com.ly.subway.subwaytools.dsfserver.utils;

import com.ly.spat.dsf.kernel.ServiceConfig;
import com.ly.spat.dsf.utils.StringUtils;
import com.ly.spat.dsf.utils.VersionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Pattern;


/**
 * DSF校验工具类
 * @author Faker
 *
 */
public class ValidTool {
	
	private final static Logger logger = LoggerFactory.getLogger(ValidTool.class);

	/**
	 * 返回为true的时候才进行挂载
	 * @param configs
	 * @return
	 */
	public static boolean checkKernelConfig(ServiceConfig configs) {
		String kernelHost = configs.getKernelHost();
		if(StringUtils.isBlank(kernelHost) || kernelHost.equals("default")) {
			logger.warn("服务连接的host与port没有设置正确，服务正常启动，不会挂载到控制中心");
			return false;
		}
		
		int port = configs.getKernelPort();
		if(port <= 0) throw new IllegalStateException("kernel port config '" + port + "' is invalid");
		
		if(!kernelHost.equals("localhost")) {
			String regex = "[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}";  
			Pattern pattern = Pattern.compile(regex);
			boolean flag = pattern.matcher(kernelHost).matches();
			
			if(!flag){
				throw new IllegalStateException("kernel host config '" + kernelHost + "' is invalid");
			}
		}
		
		return true;
	}
	
	/**
	 * 校验plevel clevel
	 * 
	 * @param config
	 */
	public static void checkConfig(ServiceConfig config) {
		
		if(StringUtils.isBlank(config.getGsName())){
			throw new IllegalStateException("DSF基础配置可能不存在，找不到gsName");
		}
		
		//转成小写
		String gsName = config.getGsName();
		config.setGsName(gsName.toLowerCase());

		//校验version
		VersionUtil.checkVersion(config.getVersion());
		
		if (config.isKernel())
			return;

		if(config.getPort() == 0) {
			throw new IllegalStateException("server port could not set to 0.");
		}
		
		if (config.getKernelPort() == config.getPort()) {
			throw new IllegalStateException("kernel port could not the same to server port.");
		}
		int plevel = config.getPlevel();
		int clevel = config.getClevel();
		if(plevel == 0) {
			plevel = 5;
		}

		if(clevel == 0) {
			clevel = 1;
		}

		if (plevel > 5) {
			throw new IllegalStateException("plevel can not big than 5");
		}

		if (plevel <= 0) {
			throw new IllegalStateException("plevel should be big than 0");
		}

		if (clevel > 5) {
			throw new IllegalStateException("clevel can not big than 5");
		}

		if (clevel <= 0) {
			throw new IllegalStateException("clevel should be big than 0");
		}
	}
}
