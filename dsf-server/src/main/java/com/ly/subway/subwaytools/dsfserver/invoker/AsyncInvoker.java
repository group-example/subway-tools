package com.ly.subway.subwaytools.dsfserver.invoker;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.MetricRegistry;
import com.ly.spat.dsf.factory.DSFThreadFactory;
import com.ly.subway.subwaytools.dsfserver.threads.TaskQueue;
import com.ly.subway.subwaytools.dsfserver.context.DSFThreadPoolExecutor;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

/**
 * 业务线程池 防止netty io线程池阻塞
 * 
 * @author lwy32937
 * @2016年6月22日 上午11:51:30
 * @param <T>
 */
public class AsyncInvoker {

	private static AsyncInvoker instance = new AsyncInvoker();

//	private static final String OK_TAG = "success";

	private ThreadPoolExecutor service;

	//default mini core threads
	private final int default_mini_thread = 10;
	
	//default max core threads
	private final int default_max_thread = 200;
	
	//默认队列10
	private final int default_accepts = 50;

	// 监控队列
	private static final MetricRegistry metrics = new MetricRegistry();

	//用gauge监控队列
	private static final Supplier<Gauge<Integer>> gauges = () -> {
		Gauge<Integer> gauge = new Gauge<Integer>() {
			@Override
			public Integer getValue() {
				return AsyncInvoker.getInvoker().getQueue().size();
			}
		};
		
		return gauge; 
	};

	private AsyncInvoker() {
	}

	public static AsyncInvoker getInvoker() {
		if (instance == null) {
			synchronized (AsyncInvoker.class) {
				if (instance == null)
					instance = new AsyncInvoker();
			}
		}

		return instance;
	}

	public void init(int coreCount, int workerCount, int accepts) {
		metrics.register("pending-jobs", gauges.get()); 
		if (workerCount == 0)
			workerCount = default_max_thread;
			
		if(coreCount == 0)
			coreCount = default_mini_thread;
		
		if(accepts == 0) {
			accepts = default_accepts;
		}
		TaskQueue<Runnable> queue = new TaskQueue<>(accepts);
		service = new DSFThreadPoolExecutor(coreCount, workerCount, 60, TimeUnit.SECONDS, queue, new DSFThreadFactory("dsfstarter-interal"));
	}

	public void addQueue(Runnable task) throws Exception {
		service.execute(task);
	}

	public void addQueue(Supplier<Runnable> s) {
		service.execute(s.get());
	}

	public ThreadPoolExecutor getService() {
		return service;
	}

	public void setService(ThreadPoolExecutor service) {
		this.service = service;
	}

	public BlockingQueue<Runnable> getQueue() {
		return service.getQueue();
	}

	/**
	 * 返回Supplier的Gauges
	 * @return
	 */
	public Supplier<Gauge<Integer>> getGauge() {
		return gauges; 
	}
}
