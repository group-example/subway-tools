package com.ly.subway.subwaytools.dsfserver.utils;

import com.ly.spat.dsf.annoation.DSFFilter;
import com.ly.spat.dsf.annoation.DSFInvoker;
import com.ly.spat.dsf.annoation.DSFService;
import com.ly.spat.dsf.filter.Filter;
import com.ly.spat.dsf.filter.FilterChain;
import com.ly.spat.dsf.invoker.ResultInvoker;
import com.ly.spat.dsf.invoker.ResultInvokerChains;
import com.ly.spat.dsf.utils.ObjectHelper;
import com.ly.spat.dsf.utils.ReflectUtils;
import com.ly.subway.subwaytools.dsfserver.startup.DSFApplicationEnv;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * 类加载工具
 *
 * @author lwy32937 2017年2月7日
 */
public class DSFLoader {

    private static final Logger logger = LoggerFactory.getLogger(DSFLoader.class);
    //所有componentScan的路径
    private final static List<String> paths = new CopyOnWriteArrayList<>();

    public static List<String> getPaths() {
        return paths;
    }

    public static void load(ClassLoader loader, String className) {
        try {
            // 用class.forname才会在类加载的时候完成一些初始化
            Class<?> cls = Class.forName(className, true, loader);
            getComponentPath(className);
            //加入配置
            Configuration cfg = cls.getAnnotation(Configuration.class);
            if (!Objects.isNull(cfg)) {
                DSFApplicationEnv.getList().add(cls);
            }

            DSFFilter df = cls.getAnnotation(DSFFilter.class);
            DSFInvoker dsfInvoker = cls.getAnnotation(DSFInvoker.class);
            // 加入filter
            if (!Objects.isNull(df) && Arrays.asList(cls.getSuperclass().getGenericInterfaces()).contains(Filter.class)) {
                FilterChain.getInstance().addFilter((Filter) cls.newInstance());
            }

            if (!Objects.isNull(dsfInvoker)) {
                ResultInvokerChains.getChains().addInvoker((ResultInvoker) cls.newInstance());
            }

            DSFService service = cls.getAnnotation(DSFService.class);
            if (!Objects.isNull(service)) {
                ReflectUtils.getServiceContract(cls);
                logger.info("DSFService cls {} finished loading", cls);
            }
        } catch (Exception e) {
            throw new IllegalStateException("loading clazz error", e);
        }


    }

	/**
	 * 加载springboot的类
	 * @param classZ
	 * @param classLoader
	 * @throws Exception
	 */
	public static void loadClassFromRoot(String classZ,ClassLoader classLoader)throws Exception
	{
		Class<?> clazz;
		try {
			clazz = Class.forName(classZ, false, classLoader);
		} catch (Exception e) {
			return;
		}
		//		DSFService service = getAnnotationClass(clazz,DSFService.class);
		//加入配置
		Class cfg = getAnnotationClass(clazz, Configuration.class);
		if (ObjectHelper.nonNull(cfg)) {
			DSFApplicationEnv.getList().add(clazz);
		}
		Class df = getAnnotationClass(clazz, DSFFilter.class);
		Class dsfInvoker = getAnnotationClass(clazz, DSFInvoker.class);
		// 加入filter
		if (ObjectHelper.nonNull(df) && Arrays.asList(clazz.getSuperclass().getGenericInterfaces()).contains(Filter.class)) {
			FilterChain.getInstance().addFilter((Filter) clazz.newInstance());
		}
		if (ObjectHelper.nonNull(dsfInvoker)) {
			ResultInvokerChains.getChains().addInvoker((ResultInvoker) clazz.newInstance());
		}
		Class service = getAnnotationClass(clazz, DSFService.class);
		if (ObjectHelper.nonNull(service)) {
		//	Objects.requireNonNull(reflectUtils, "reflectUtils not instance, please try to init object first.");
			ReflectUtils.getServiceContract(service);
			logger.info("DSFService {} loaded finish", clazz);
		}
	}

	/**
	 * 传入的参数为annotationClass，此为注解类
	 * 从srcClass上找注解，若找不到，从所有实现接口上找注解
	 * @param srcClass
	 * @param annotationClass
	 * @param <T>
	 * @return
	 */
	public static <T> Class getAnnotationClass(Class srcClass,Class<T> annotationClass) {
		//		//find annotation from srcClass
		//		T resultAnnotation=(T)srcClass.getAnnotation(annotationClass);
		//		if(resultAnnotation!=null)
		//			return resultAnnotation;
		T resultAnnotation=null;
		//从所有实现接口上找注解
		Class[]interfaces=srcClass.getInterfaces();
		for(Class interfaceItem:interfaces)
		{
			resultAnnotation=(T)interfaceItem.getAnnotation(annotationClass);
			if(resultAnnotation!=null) {
				logger.info("resultAnnnotation is:"+srcClass.getName());
				return interfaceItem;
			}
		}
		return null;
	}



	private static void getComponentPath(String className) throws ClassNotFoundException {
        Class<?> cls = Class.forName(className, true, Thread.currentThread().getContextClassLoader());
        ComponentScan scan = cls.getAnnotation(ComponentScan.class);
        if (!Objects.isNull(scan)) {
            String[] scanPaths = scan.value();
            List<String> pathList = Arrays.asList(scanPaths);
            pathList.forEach(path -> {
                if (!paths.contains(path)) {
                    if (paths.isEmpty()) {
                        paths.add(path);
                    }
                    paths.forEach(p -> {
                        if (path.startsWith(p)) {
                            return;
                        } else if (p.startsWith(path)) {
                            paths.remove(p);
                            paths.add(path);
                        } else {
                            paths.add(path);
                        }
                    });
                }
            });
        }
    }

    /**
     * 加载真正的契约
     *
     * @param path
     */
    @Deprecated
    public static void getReflectInfo(String path) {
        try {
            Class<?> clazz = Class.forName("com.ly.spat.dsf.utils.ClassUtils");
            Method method = clazz.getDeclaredMethod("loadBean", String.class, String.class);
            String path_ = path.replace(".", "/");
            method.setAccessible(true);
            method.invoke(clazz.newInstance(), path_, path);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }

    }
}
