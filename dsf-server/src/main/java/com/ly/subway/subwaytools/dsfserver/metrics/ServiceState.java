package com.ly.subway.subwaytools.dsfserver.metrics;

import java.util.List;

/**
 * 服务组状态
 * @author lwy32937
 * 2016年9月6日 下午2:48:19
 */
public class ServiceState {

	private String GSName; 
	
	private String Version; 
	
	private int CLevel; 
	
	private double UsedMemory; 
	
	private double UsedCPU; 
	
	private int ThreadCount; 
	
	private List<ServiceEndpoint> Endpoints;

	public String getGSName() {
		return GSName;
	}

	public void setGSName(String gSName) {
		GSName = gSName;
	}

	public String getVersion() {
		return Version;
	}

	public void setVersion(String version) {
		Version = version;
	}

	public int getCLevel() {
		return CLevel;
	}

	public void setCLevel(int cLevel) {
		CLevel = cLevel;
	}

	public double getUsedMemory() {
		return UsedMemory;
	}

	public void setUsedMemory(double usedMemory) {
		UsedMemory = usedMemory;
	}

	public double getUsedCPU() {
		return UsedCPU;
	}

	public void setUsedCPU(double usedCPU) {
		UsedCPU = usedCPU;
	}

	public int getThreadCount() {
		return ThreadCount;
	}

	public void setThreadCount(int threadCount) {
		ThreadCount = threadCount;
	}

	public List<ServiceEndpoint> getEndpoints() {
		return Endpoints;
	}

	public void setEndpoints(List<ServiceEndpoint> endpoints) {
		Endpoints = endpoints;
	}

	
}
