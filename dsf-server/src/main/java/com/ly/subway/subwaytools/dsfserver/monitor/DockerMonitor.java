package com.ly.subway.subwaytools.dsfserver.monitor;

import com.ly.spat.dsf.factory.DSFThreadFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * get docker cpu mem states
 *
 * @author lwy32937
 * @create 2017/11/20
 **/
public final class DockerMonitor {

    private static final Logger log = LoggerFactory.getLogger(DockerMonitor.class);

    private static final ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor(new DSFThreadFactory("dsfstarter-docker", true));

    private static final String CPUFILE = "/sys/fs/cgroup/cpuacct/cpuacct.usage";

    private static final String SYSTEMCPUFILE = "/proc/stat";

    private static final String MEMFILE = "/sys/fs/cgroup/memory/memory.stat";

    public static final String SYSTEMTIME = "system";

    public static final String USERTIME = "user";

    public static final String MEMCACHE = "cache";

    public static final String MEMRSS = "rss";

    public static final String MEMSWAP = "swap";

    public static final String MEMFREE = "free";

    public static final String MEMLIMIT = "hierarchical_memory_limit";

    //always change
    private DockerStates states;

    private long lastTime;

    public DockerMonitor(Builder builder) {
        states = builder.states;
    }

    public Builder newBuilder() {
        return new Builder(this);
    }

    public DockerStates getStates() {
        return states = getMetrics();
    }

    public static class Builder {

        private DockerStates states;

        public Builder() {
        }

        public Builder setStates(DockerStates states) {
            this.states = states;
            return this;
        }

        public Builder(DockerMonitor monitor) {
            this.states = monitor.states;
        }

        public DockerMonitor build() {
            return new DockerMonitor(this);
        }
    }


    public DockerStates getMetrics() {
        //cpu
        try {
            if (lastTime == 0) {
                lastTime = System.nanoTime();

                //取上次cpusage
                double firstUserCpu = getUserCpusage();
                double firstSysCpu = getSysCpusage();

                if (ObjectUtil.nonNull(states)) {
                    states = states.newBuilder().setLastTime(lastTime).setPreviousSysCpu(firstSysCpu).setPreviousUserCpu(firstUserCpu).build();
                }else {
                    states = new DockerStates.Builder().setLastTime(lastTime).setPreviousUserCpu(firstUserCpu).setPreviousSysCpu(firstSysCpu).build();
                }

                return states;
            }
        } catch (Exception e) {
            log.error("get metrics error", e);
        }

        DockerStates.Cpusage.Builder cpuBuilder = new DockerStates.Cpusage.Builder();
        try {
            if (lastTime != 0) {
                cpuBuilder.setUserCpusage(getCpuPercent(states.getPreviousUserCpusage(), states.getPreviousSystemCpusage()));
            }
        } catch (Exception e) {
            log.error("get metrics error", e);
        }

        //mem
        DockerStates.Memusage.Builder memBuilder = new DockerStates.Memusage.Builder();
        BufferedReader memReader = null;
        try {
            memReader = new BufferedReader(new FileReader(new File(MEMFILE)));
            String line = null;
            while (ObjectUtil.nonNullOrEmpty(line = memReader.readLine())) {
                getMemory(line, memBuilder);
            }
        } catch (FileNotFoundException e) {
            log.error("error", e);
        } catch (IOException e) {
            log.error("error", e);
        } finally {
            if (ObjectUtil.nonNull(memReader)) {
                try {
                    memReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return states.newBuilder().setMemusage(memBuilder.build()).setCpuage(cpuBuilder.build()).build();
    }

    /**
     * 计算CPU使用率真的公式
     * @return 带有百分比
     */
    private double getCpuPercent(double previousUserCpu, double previousSysCpu) {
        double rate = 0;
        try {
            int core = Runtime.getRuntime().availableProcessors();
            long nowTime = System.nanoTime();
            double userCpu = getUserCpusage();
            double sysCpu = getSysCpusage();

            double realUserCpu = userCpu - previousUserCpu;
            double realSysCpu = sysCpu - previousSysCpu;
            rate = realUserCpu * core * 100 / realSysCpu / 10000000;
            log.info("lastTime:{}, previousUserCpu:{}, previousSysCpu:{}, nowTime:{}, userCpu:{}, sysCPu:{}, realUserCpu:{}, reaslSysCpu:{}, core count:{}, rate:{}", lastTime, previousUserCpu, previousSysCpu, nowTime, userCpu, sysCpu, realUserCpu, realSysCpu, core, rate);
            states = states.newBuilder().setPreviousUserCpu(userCpu).setPreviousSysCpu(sysCpu).build();
        } catch (Exception e) {
            e.printStackTrace();

        }


        return Double.valueOf(String.format("%1$.2f", rate));
    }


    /**
     * user 利用率
     *
     * @return
     * @throws Exception
     */
    private double getUserCpusage() throws Exception {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(new File(CPUFILE)));
            String line = null;
            String res = null;
            while ((line = reader.readLine()) != null) {
                res = line;
            }

            return Double.valueOf(res);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                reader.readLine();
            }
        }

        return 0l;
    }

    private double getSysCpusage() throws Exception {
        double total = 0;
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(new File(SYSTEMCPUFILE)));
            String res = reader.readLine();

            if (ObjectUtil.nonNullOrEmpty(res)) {
                String cpus[] = res.split(" ");
                List<String> list = new ArrayList<>();
                for(int i=1; i<cpus.length; i++) {
                    list.add(cpus[i]);
                }

                total = list.stream().mapToDouble(cpu -> {
                    if (ObjectUtil.nullOrEmpty(cpu)) {
                        return 0;
                    }
                    return Double.valueOf(cpu);
                }).sum();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                reader.close();
            }
        }

        return total;
    }

    private DockerStates.Memusage.Builder getMemory(String line, DockerStates.Memusage.Builder builder) {
        String[] values = line.split(" ");
        String key = values[0];
        String value = values[1];
        ObjectUtil.requireNonNullOrEmpty(key, "mem metrics key");

        if (key.equals(MEMCACHE)) {
            builder.setCache(Double.valueOf(value) / 1024 / 1024 / 1024);
        }
        if (key.equals(MEMRSS)) {
            builder.setRss(Double.valueOf(value) / 1024 / 1024 / 1024);
        }

        if (key.equals(MEMSWAP)) {
            builder.setSwap(Double.valueOf(value) / 1024 / 1024 / 1024);
        }

        if (key.equals(MEMLIMIT)) {
            builder.setLimit(Double.valueOf(value) / 1024 / 1024 / 1024);
        }

        return builder;
    }

}
