package com.ly.subway.subwaytools.dsfserver.metrics;

import java.util.List;

/**
 * 服务状态
 * @author lwy32937
 * 2016年9月6日 下午2:41:35
 */
public class ServiceStateWrapper {

	private String HostIP; 
	
	private double UsedMemory; 
	
	private double UsedCPU;
	
	private List<ServiceState> Groups;

	public String getHostIP() {
		return HostIP;
	}

	public void setHostIP(String hostIP) {
		HostIP = hostIP;
	}

	public double getUsedMemory() {
		return UsedMemory;
	}

	public void setUsedMemory(double usedMemory) {
		UsedMemory = usedMemory;
	}

	public double getUsedCPU() {
		return UsedCPU;
	}

	public void setUsedCPU(double usedCPU) {
		UsedCPU = usedCPU;
	}

	public List<ServiceState> getGroups() {
		return Groups;
	}

	public void setGroups(List<ServiceState> groups) {
		Groups = groups;
	}
	
}
