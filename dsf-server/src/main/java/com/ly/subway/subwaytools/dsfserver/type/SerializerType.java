package com.ly.subway.subwaytools.dsfserver.type;

/**
 * 序列化
 * @author lwy32937
 *
 */
public enum SerializerType {

	JSON((byte)1),

	JAVABinary((byte)2);

	private byte type;

	private SerializerType(byte type) {
		this.type = type;
	} 
	
	public byte getType() {
		return this.type; 
	}

}
