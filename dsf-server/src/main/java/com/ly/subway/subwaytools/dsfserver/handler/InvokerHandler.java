package com.ly.subway.subwaytools.dsfserver.handler;

import com.codahale.metrics.ConsoleReporter;
import com.codahale.metrics.Counter;
import com.codahale.metrics.Meter;
import com.codahale.metrics.Timer;
import com.ly.spat.dsf.annoation.DSFAction;
import com.ly.spat.dsf.annoation.HttpType;
import com.ly.spat.dsf.constants.Constants;
import com.ly.spat.dsf.context.DSFContext;
import com.ly.spat.dsf.entity.ClassInfo;
import com.ly.spat.dsf.entity.MethodInfo;
import com.ly.spat.dsf.entity.MetricsKey;
import com.ly.spat.dsf.function.MessageBuilder;
import com.ly.spat.dsf.handler.Handler;
import com.ly.spat.dsf.invoker.ResultInvoker;
import com.ly.spat.dsf.invoker.ResultInvokerChains;
import com.ly.spat.dsf.utils.ReflectUtils;
import com.ly.subway.subwaytools.dsfserver.invoker.AsyncInvoker;
import com.ly.subway.subwaytools.dsfserver.metrics.RequestMetrics;
import com.ly.subway.subwaytools.dsfserver.utils.WriteUtils;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.QueryStringDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 真正的服务调用
 * 
 * @author lwy32937
 * @2016年6月30日 上午9:35:38
 */
public class InvokerHandler extends AbstractHandler implements Handler {

	private static final Logger logger = LoggerFactory.getLogger(InvokerHandler.class);

	private static AsyncInvoker invoker = AsyncInvoker.getInvoker();

	private static final Map<MetricsKey, Meter> meterMaps = new ConcurrentHashMap<>();

	private static final Map<MetricsKey, Timer> timerMaps = new ConcurrentHashMap<>();

	private static final Map<MetricsKey, Timer.Context> contextMaps = new ConcurrentHashMap<>();

	private static final Map<MetricsKey, AtomicLong> successCounters = new ConcurrentHashMap<>();

	private static final Map<MetricsKey, AtomicLong> failCounters = new ConcurrentHashMap<>();

	@Autowired
	private ResultInvokerChains chains;

	@Override
	public void handle(DSFContext ctx) {
		logger.trace("add task ");
		try {
			invoker.addQueue(new Invoker(meterMaps, timerMaps, contextMaps, successCounters, failCounters, ctx));
		} catch (Exception e) {
			logger.error("Internal Server Error.", e);
			// 服务器内部错误
			ctx.getChannel().writeAndFlush(buildErrResponse("Internal Server Error.", HttpResponseStatus.INTERNAL_SERVER_ERROR, ctx));
		}
	}

	@Override
		public void handlePost(DSFContext ctx) throws Exception {
		;
	}

	@Override
	public void handleGet(DSFContext ctx) throws Exception {
		;
	}

	public static Map<MetricsKey, Meter> getMetermaps() {
		return meterMaps;
	}

	public static Map<MetricsKey, Timer> getTimermaps() {
		return timerMaps;
	}

	public static Map<MetricsKey, AtomicLong> getSuccesscounters() {
		return successCounters;
	}

	public static Map<MetricsKey, AtomicLong> getFailcounters() {
		return failCounters;
	}

}

/**
 * 业务逻辑处理
 * 
 * @author lwy32937 2016年8月11日 下午3:22:10
 */
class Invoker implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(Invoker.class);

	private static final String charset = "utf-8";

	public DSFContext ctx;

	private final MetricsKey measureKey;

	private ReentrantLock lock = new ReentrantLock();

	private final Map<MetricsKey, Meter> meterMaps;

	private final Map<MetricsKey, Timer> timerMaps;

	private final Map<MetricsKey, Timer.Context> contextMaps;

	private final Map<MetricsKey, AtomicLong> successCounters;

	private final Map<MetricsKey, AtomicLong> failCounters;

	private final Object object = new Object();

	private AtomicLong successCounter;

	private AtomicLong failCounter;

	private ResultInvokerChains chains = ResultInvokerChains.getChains();

	public Invoker(Map<MetricsKey, Meter> meterMaps, Map<MetricsKey, Timer> timerMaps,
			Map<MetricsKey, Timer.Context> contextMaps, Map<MetricsKey, AtomicLong> successCounters,
			Map<MetricsKey, AtomicLong> failCounters, DSFContext ctx) {
		this.meterMaps = meterMaps;
		this.timerMaps = timerMaps;
		this.contextMaps = contextMaps;
		this.successCounters = successCounters;
		this.failCounters = failCounters;
		this.ctx = ctx;
		String action = ctx.getInvocation().getInfo().getMethodInfo().get(ctx.getInvocation().getUri()).getPath();
		measureKey = new MetricsKey(ctx.getInvocation().getInfo().getSname(), action, ctx.getcName(),ctx.getRequestIp());
	}

	@Override
	public void run() {
		// metrics
		Timer.Context context = initMetrics();
		try {
			doInvoke(ctx, getRequestMethod(ctx));
		} catch (Exception e) {
			failCounter.incrementAndGet();
			logger.error(Constants.SERVICE_INTERNAL_ERROR + " client ip :{}", ctx.getRequestIp(), e);
			WriteUtils.write(error(e),	HttpResponseStatus.INTERNAL_SERVER_ERROR, ctx);
		} finally {
			successCounter.incrementAndGet();
			context.stop();
		}
	}

	private String error(Exception e){
		StringBuilder sb = new StringBuilder();
		sb.append(Constants.SERVICE_INTERNAL_ERROR).append(e);
		if(!Objects.isNull(e.getCause())) {
			sb.append(", caused by: ").append(e.getCause());
		}

		return sb.toString();
	}

	private RequestMetrics<Meter> rm = (measureKey) -> {
		return RequestMetrics.metrics.meter(measureKey.toString() + ".meter");
	};

	private RequestMetrics<Timer> rt = (measureKey) -> {
		return RequestMetrics.metrics.timer(measureKey.toString() + ".timer");
	};

	private RequestMetrics<Counter> succ = (measureKey) -> {
		return RequestMetrics.metrics.counter(measureKey.toString() + ".succ_counter");
	};

//	private RequestMetrics<AtomicLong> fail = (measureKey) -> {
//		return RequestMetrics.metrics.counter(measureKey.toString() + "fail_counter");
//	};

	static void startReport() {
		ConsoleReporter reporter = ConsoleReporter.forRegistry(RequestMetrics.metrics).convertRatesTo(TimeUnit.SECONDS)
				.convertDurationsTo(TimeUnit.MILLISECONDS).build();
		reporter.start(1, TimeUnit.SECONDS);
	}

	Timer.Context initMetrics() {
//		startReport();
		Meter meter = meterMaps.get(measureKey);
		successCounter = successCounters.get(measureKey);
		failCounter = failCounters.get(measureKey);
		Timer timer = timerMaps.get(measureKey);

		if (Objects.isNull(successCounter)) {
			successCounter = new AtomicLong(0);
			successCounters.putIfAbsent(measureKey, successCounter);
		}

		if (Objects.isNull(meter)) {
			meter = rm.apply(measureKey);
			meterMaps.putIfAbsent(measureKey, meter);
		}

		meter.mark();

		if (Objects.isNull(timer)) {
			timer = rt.apply(measureKey);
			timerMaps.putIfAbsent(measureKey, timer);
		}

		// 进行调用次数统计
//		successCounter.inc();

		if (Objects.isNull(failCounter)) {
//			failCounter = fail.apply(measureKey);
			failCounter = new AtomicLong(0);
			failCounters.putIfAbsent(measureKey, failCounter);
		}
		
		return timer.time();
	}

	/**
	 * 处理httpRequest中的参数
	 * 
	 * @return
	 */
	private Map<String, List<String>> getParams(String uri) {
		// 开始函数调用
		QueryStringDecoder decoder = new QueryStringDecoder(uri, Charset.forName(charset));
		Map<String, List<String>> params = decoder.parameters();
		return params;
	}

	// real invoke
	private void doInvoke(DSFContext ctx, MethodInfo methodInfo) throws Exception {

		Object result = null;
		Throwable t = null;
		try {
			chains.getInvokers().forEach(invoker -> {
				invoker.beforeInvoke(ctx);
			});

			Object res = null;
			Map<String, List<String>> params = getParams(ctx.getRequest().uri());
			ReflectUtils.processWithParams(ctx.getRequest().uri(), params, ctx, methodInfo);
			Method method = methodInfo.getMethod();
			DSFAction dsfAction = method.getAnnotation(DSFAction.class);
			HttpType httpType = dsfAction.produces();
//			logger.info("请求的参数：{}, method:{}, clazz:{}", methodInfo.getArgs(), method.getName(), ctx.getInvocation().getObj());
			result = method.invoke(ctx.getInvocation().getObj(), methodInfo.getArgs());
			Class<?> type = method.getReturnType();
			if (type.isPrimitive()) {
				res = ReflectUtils.getPrimeValue(result, type, res);
			} else {
				res = type.cast(result);
			}

			ctx.getWatch().stop();
			WriteUtils.write(res, HttpResponseStatus.OK, ctx, httpType);
		}catch(Exception e) {
			t = e;
			throw e;
		}finally {
			List<ResultInvoker> list = chains.getInvokers();
			for(ResultInvoker invoker: list) {
				invoker.afterInvoke(ctx, result, t);
			}
		}

	}

	/**
	 * 取出method path
	 *
	 * @param ctx
	 * @return
	 */
	private MethodInfo getRequestMethod(DSFContext ctx) {
		ClassInfo info = ReflectUtils.getClassInfo(ctx.getInvocation().getInfo().getCls());
		MethodInfo methodInfo = info.getMethodInfo().get(ctx.getInvocation().getUri());

		MethodInfo resultInfo = new MethodInfo();
		BeanUtils.copyProperties(methodInfo, resultInfo);
		return resultInfo;
	}

	/**
	 * 函数式编程 构建错误消息
	 */
	protected MessageBuilder<String> msgBuilder = (e, string) -> {
		StringBuilder sb = new StringBuilder();
		sb.append(string);
		Optional<Throwable> optional = Optional.of(e);
		if (optional.isPresent())
			sb.append(e.getMessage());

		return sb.toString();
	};

}
