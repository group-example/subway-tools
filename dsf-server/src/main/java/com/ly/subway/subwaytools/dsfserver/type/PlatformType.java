package com.ly.subway.subwaytools.dsfserver.type;

/**
 * plat form
 * @author lwy32937
 *
 */
public enum PlatformType {

	JAVA, 
	
	DOTNET, 
	
	GOLANG
}
