package com.ly.subway.subwaytools.dsfserver.listener;


import com.ly.spat.dsf.DSFApplication;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * ServletContext 初始化
 * Created by lwy32937 on 2017/7/28.
 */
public class DSFContextLoaderListener implements ServletContextListener{

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext sc = sce.getServletContext();
        String gsName = sc.getInitParameter("gsName");
        DSFApplication.run(gsName);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
