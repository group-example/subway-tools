package com.ly.subway.subwaytools.dsfserver.monitor;

/**
 * get "/sys/fs/cgroup", docker monitor, cpu, mem, io
 * @author lwy32937
 * @create 2017/11/20
 **/
public class DockerStates {

    private Cpusage cpusage; //

    private Memusage memusage; // dourble

    private long lastTime;

    private double previousUserCpusage;

    private double previousSystemCpusage;

    public DockerStates(Builder builder) {
        this.cpusage = builder.cpusage;
        this.memusage = builder.memusage;
        this.lastTime = builder.lastTime;
        this.previousSystemCpusage = builder.previousSystemCpusage;
        this.previousUserCpusage = builder.previousUserCpusage;
    }

    public Builder newBuilder() {
        return new Builder(this);
    }

    public Cpusage getCpusage() {
        return cpusage;
    }

    public Memusage getMemusage() {
        return memusage;
    }

    public long getLastTime() {
        return lastTime;
    }

    public double getPreviousUserCpusage() {
        return previousUserCpusage;
    }

    public double getPreviousSystemCpusage() {
        return previousSystemCpusage;
    }

    public static class Builder {
        private Cpusage cpusage;

        private Memusage memusage;

        private long lastTime;

        private double previousUserCpusage;

        private double previousSystemCpusage;

        public Builder() {
        }

        public Builder(DockerStates states) {
            this.cpusage = states.cpusage;
            this.memusage = states.memusage;
            this.lastTime = states.lastTime;
            this.previousSystemCpusage = states.previousSystemCpusage;
            this.previousUserCpusage = states.previousUserCpusage;
        }

        public Builder setCpuage(Cpusage cpuage) {
            this.cpusage = cpuage;
            return this;
        }

        public Builder setMemusage(Memusage memusage) {
            this.memusage = memusage;
            return this;
        }

        public Builder setLastTime(long lastTime) {
            this.lastTime = lastTime;
            return this;
        }
        public Builder setPreviousSysCpu(double previousSysCpu) {
            this.previousSystemCpusage = previousSysCpu;
            return this;
        }
        public Builder setPreviousUserCpu(double previousUserCpu) {
            this.previousUserCpusage = previousUserCpu;
            return this;
        }

        public DockerStates build() {
            return new DockerStates(this);
        }
    }

    public static class Cpusage {

        private double cpusage;

        public Cpusage(Builder builder) {
            this.cpusage = builder.cpusage;
        }

        public double getCpusage() {
            return cpusage;
        }

        public static class Builder {
            private double cpusage;

            public Builder() {

            }

            public Builder setUserCpusage(double cpusage) {
                this.cpusage = cpusage;
                return this;
            }


            public Cpusage build() {
                return new Cpusage(this);
            }
        }
    }

    public static class Memusage {

        private double cache;

        private double rss;

        private double swap;

        private double free;

        private double limit;

        public Memusage(Builder builder) {
            this.cache = builder.cache;
            this.rss = builder.rss;
            this.swap = builder.swap;
            this.free = builder.free;
            this.limit = builder.limit;
        }

        public double getCache() {
            return cache;
        }

        public double getRss() {
            return rss;
        }

        public double getSwap() {
            return swap;
        }

        public double getFree() {
            return free;
        }

        public double getLimit() {
            return limit;
        }

        public static class Builder {
            private double cache;

            private double rss;

            private double swap;

            private double free;

            private double limit;

            public Builder(){}

            public Builder setCache(double cache) {
                this.cache = cache;
                return this;
            }

            public Builder setRss(double rss) {
                this.rss = rss;
                return this;
            }

            public Builder setSwap(double swap) {
                this.swap = swap;
                return this;
            }

            public Builder setFree(double free) {
                this.free = free;
                return this;
            }

            public Builder setLimit(double limit) {
                this.limit = limit;
                return this;
            }

            public Memusage build() {
                return new Memusage(this);
            }

        }
    }
}




