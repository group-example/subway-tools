package com.ly.spat.dsf;

import com.ly.subway.subwaytools.dsfserver.DSF;
import com.ly.subway.subwaytools.dsfserver.startup.DSFApplicationEnv;
import com.ly.subway.subwaytools.dsfserver.utils.ClassUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

/**
 * DSFApp
 *
 * @author lwy32937
 * @2016年6月29日 下午2:15:03
 */
public class DSFApplication {

    private static final Logger logger = LoggerFactory.getLogger(DSFApplication.class);

    public static void run(String name) {
        try {
            logger.debug("service starting");
            ClassUtils utils = new ClassUtils();
            //加载类 初始化spring 容器
            DSFApplicationEnv.getList().add(DSF.class);
            utils.loadBean();
            ApplicationContext applicationContext = DSFApplicationEnv.getContext();
            DSF d = applicationContext.getBean(DSF.class);
            d.init(name);
        } catch (Exception e) {
            logger.error("init failed", e);
            System.exit(0);
        }
    }
}
