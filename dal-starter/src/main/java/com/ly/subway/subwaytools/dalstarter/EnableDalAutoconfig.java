package com.ly.subway.subwaytools.dalstarter;

import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import com.ly.dal.datasource.RoutableDataSource;
import com.ly.tcbase.config.AppProfile;
import org.apache.ibatis.plugin.Interceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.ResourceLoader;

@Configuration
@EnableConfigurationProperties(MybatisProperties.class)
@ConditionalOnProperty(prefix = "mybatis",
        value = "enabled",
        matchIfMissing = true)
public class EnableDalAutoconfig {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private MybatisProperties mybatisProperties;

    @Autowired
    private Interceptor[] interceptors;

//    @Autowired
//    private DatabaseIdProvider databaseIdProvider;

    @Autowired
    private RoutableDataSource dataSource;


    private ResourceLoader resourceLoader = new DefaultResourceLoader();

    /**
     * 同程dal框架数据源
     *
     * @return
     */
    @Primary
    @Bean(initMethod = "init", destroyMethod = "close")
    @Order(0)
    public RoutableDataSource druidDataSource() {
        RoutableDataSource dataSource = new RoutableDataSource();
        dataSource.setEnv(AppProfile.getEnvironment());
        dataSource.setProjectId(AppProfile.getAppName());
        dataSource.setDbName(mybatisProperties.getDbName());
//        this.dataSource=dataSource;
        return dataSource;
    }

    /**
     * druid访问规则
     *
     * @return
     */
    @Bean
    public ServletRegistrationBean druidServlet() {
        ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(new StatViewServlet(), "/druid/*");
        //白名单：
//        servletRegistrationBean.addInitParameter("allow", "127.0.0.1");
        //IP黑名单 (存在共同时，deny优先于allow) : 如果满足deny的话提示:Sorry, you are not permitted to view this page.
//        servletRegistrationBean.addInitParameter("deny", "192.168.1.73");
        //登录查看信息的账号密码.
        servletRegistrationBean.addInitParameter("loginUsername", "admin");
        servletRegistrationBean.addInitParameter("loginPassword", "123456");
        //是否能够重置数据.
        servletRegistrationBean.addInitParameter("resetEnable", "false");
        return servletRegistrationBean;
    }

    /**
     * web容器过滤规则
     *
     * @return
     */
    @Bean
    public FilterRegistrationBean filterRegistrationBean() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        filterRegistrationBean.setFilter(new WebStatFilter());
        filterRegistrationBean.addUrlPatterns("/*");
        filterRegistrationBean.addInitParameter("exclusions", "*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*");
        return filterRegistrationBean;
    }


    /**
     * mybatis-plus分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }

    /**
     * mybatis的SqlSessionFactory
     *
     * @return
     */
    @Bean
    @Order
    public MybatisSqlSessionFactoryBean mybatisSqlSessionFactoryBean() {
        MybatisSqlSessionFactoryBean mybatisPlus = new MybatisSqlSessionFactoryBean();
        mybatisPlus.setDataSource(dataSource);
//        mybatisPlus.setVfs(SpringBootVFS.class);
        mybatisPlus.setConfigLocation(this.resourceLoader.getResource(this.mybatisProperties.getConfigLocation()));
        if (!ObjectUtils.isEmpty(this.interceptors)) {
            mybatisPlus.setPlugins(this.interceptors);
        }

        mybatisPlus.setPlugins(interceptors);
        mybatisPlus.setTypeAliasesPackage(mybatisProperties.getTypeAliasesPackage());

        mybatisPlus.setMapperLocations(mybatisProperties.resolveMapperLocations());
        return mybatisPlus;
    }

    /**
     * mybatis的mapper扫描路径
     *
     * @return
     */
//    @Bean
//    @Order
//    public MapperScannerConfigurer mapperScannerConfigurer() {
//        MapperScannerConfigurer mapperScannerConfigurer = new MapperScannerConfigurer();
//        mapperScannerConfigurer.setBasePackage(mybatisProperties.getBasePackage());
//        return mapperScannerConfigurer;
//    }

}

